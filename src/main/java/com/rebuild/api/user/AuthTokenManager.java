/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.api.user;

import cn.devezhao.commons.CodecUtils;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.Application;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;


@Slf4j
public class AuthTokenManager {

    
    private static final String TOKEN_PREFIX = "RBLT.";

    
    public static final int TOKEN_EXPIRES = 60 * 60 * 2;
    
    public static final int H5TOKEN_EXPIRES = 60 * 60 * 12;

    
    public static String generateToken(ID user, int expires) {
        String token = String.format("%s,%d,%s,v1",
                user, System.currentTimeMillis(), CodecUtils.randomCode(10));
        token = CodecUtils.base64UrlEncode(token);  
        Application.getCommonsCache().putx(TOKEN_PREFIX + token, user, expires);
        return token;
    }

    
    public static ID verifyToken(String token, boolean verifyAfterDestroy) {
        if (StringUtils.isBlank(token)) return null;

        token = TOKEN_PREFIX + token;
        ID user = (ID) Application.getCommonsCache().getx(token);
        if (user != null && verifyAfterDestroy) {
            log.warn("Destroy token : {}", token);
            Application.getCommonsCache().evict(token);
        }
        return user;
    }

    
    public static ID refreshToken(String token, int expires) {
        ID user = verifyToken(token, false);
        if (user == null)  return null;

        Application.getCommonsCache().putx(TOKEN_PREFIX + token, user, expires);
        return user;
    }
}
