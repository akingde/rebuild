/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.api;

import com.alibaba.fastjson.JSON;


public abstract class Controller {

    
    public static final int CODE_OK = 0;

    
    public static final int CODE_ERROR = 400;

    
    public static final int CODE_SERV_ERROR = 500;

    
    protected JSON formatSuccess(Object data) {
        return RespBody.ok(data).toJSON();
    }

    
    protected JSON formatFailure(String errorMsg) {
        return formatFailure(errorMsg, CODE_ERROR);
    }

    
    protected JSON formatFailure(String errorMsg, int errorCode) {
        return RespBody.error(errorMsg, errorCode).toJSON();
    }
}
