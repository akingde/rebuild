/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.configuration;

import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSON;
import com.rebuild.core.configuration.general.BaseLayoutManager;

import java.util.ArrayList;
import java.util.List;


public class NavManager extends BaseLayoutManager {

    
    public static final String NAV_PARENT = "$PARENT$";
    
    public static final String NAV_FILEMRG = "$FILEMRG$";
    
    public static final String NAV_FEEDS = "$FEEDS$";
    
    public static final String NAV_PROJECT = "$PROJECT$";

    
    public static final String NAV_DIVIDER = "$DIVIDER$";

    public static final NavManager instance = new NavManager();

    protected NavManager() {
    }

    
    public JSON getNavLayout(ID user) {
        ConfigBean config = getLayoutOfNav(user);
        return config == null ? null : config.toJSON();
    }

    
    public JSON getNavLayoutById(ID cfgid) {
        ConfigBean config = getLayoutById(cfgid);
        return config == null ? null : config.toJSON();
    }

    
    public ID[] getUsesNavId(ID user) {
        Object[][] uses = getUsesConfig(user, null, TYPE_NAV);
        List<ID> array = new ArrayList<>();
        for (Object[] c : uses) {
            array.add((ID) c[0]);
        }
        return array.toArray(new ID[0]);
    }
}
