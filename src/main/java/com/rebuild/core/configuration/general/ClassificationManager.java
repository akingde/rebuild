/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.configuration.general;

import cn.devezhao.commons.ObjectUtils;
import cn.devezhao.persist4j.Field;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.Application;
import com.rebuild.core.configuration.ConfigManager;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.metadata.easymeta.EasyMetaFactory;
import com.rebuild.core.metadata.impl.EasyFieldConfigProps;


public class ClassificationManager implements ConfigManager {

    public static final ClassificationManager instance = new ClassificationManager();

    private ClassificationManager() {
    }

    public static final int BAD_CLASSIFICATION = -1;

    
    public String getName(ID itemId) {
        String[] ns = getItemNames(itemId);
        return ns == null ? null : ns[0];
    }

    
    public String getFullName(ID itemId) {
        String[] ns = getItemNames(itemId);
        return ns == null ? null : ns[1];
    }

    
    private String[] getItemNames(ID itemId) {
        final String ckey = "ClassificationNAME-" + itemId;
        String[] cached = (String[]) Application.getCommonsCache().getx(ckey);
        if (cached != null) {
            return cached[0].equals(DELETED_ITEM) ? null : cached;
        }

        Object[] o = Application.createQueryNoFilter(
                "select name,fullName from ClassificationData where itemId = ?")
                .setParameter(1, itemId)
                .unique();
        if (o != null) cached = new String[]{(String) o[0], (String) o[1]};
        
        if (cached == null) cached = new String[]{DELETED_ITEM, DELETED_ITEM};

        Application.getCommonsCache().putx(ckey, cached);
        return cached[0].equals(DELETED_ITEM) ? null : cached;
    }

    
    public ID findItemByName(String name, Field field) {
        ID dataId = getUseClassification(field, false);
        if (dataId == null) {
            return null;
        }

        
        String ql = String.format(
                "select itemId from ClassificationData where dataId = '%s' and fullName like '%%%s'", dataId, name);
        Object[][] hasMany = Application.createQueryNoFilter(ql).array();
        if (hasMany.length == 0) {
            return null;
        } else if (hasMany.length == 1) {
            return (ID) hasMany[0][0];
        } else {
            
            return (ID) hasMany[0][0];
        }
    }

    
    public int getOpenLevel(Field field) {
        ID dataId = getUseClassification(field, false);
        if (dataId == null) {
            return BAD_CLASSIFICATION;
        }

        String ckey = "ClassificationLEVEL-" + dataId;
        Integer cLevel = (Integer) Application.getCommonsCache().getx(ckey);
        if (cLevel == null) {
            Object[] o = Application.createQueryNoFilter(
                    "select openLevel from Classification where dataId = ?")
                    .setParameter(1, dataId)
                    .unique();

            cLevel = o == null ? BAD_CLASSIFICATION : (Integer) o[0];
            Application.getCommonsCache().putx(ckey, cLevel);
        }

        
        String specLevel = EasyMetaFactory.valueOf(field).getExtraAttr(EasyFieldConfigProps.CLASSIFICATION_LEVEL);
        int specLevelAsInt = ObjectUtils.toInt(specLevel, -1);
        if (specLevelAsInt > BAD_CLASSIFICATION && specLevelAsInt <= cLevel) {
            return specLevelAsInt;
        }

        return cLevel;
    }

    
    public ID getUseClassification(Field field, boolean verfiy) {
        String classUse = EasyMetaFactory.valueOf(field).getExtraAttr(EasyFieldConfigProps.CLASSIFICATION_USE);
        ID dataId = ID.isId(classUse) ? ID.valueOf(classUse) : null;
        if (dataId == null) {
            return null;
        }

        if (verfiy && getOpenLevel(field) == BAD_CLASSIFICATION) {
            return null;
        }
        return dataId;
    }

    @Override
    public void clean(Object cid) {
        ID id2 = (ID) cid;
        if (id2.getEntityCode() == EntityHelper.ClassificationData) {
            Application.getCommonsCache().evict("ClassificationNAME-" + cid);
        } else if (id2.getEntityCode() == EntityHelper.Classification) {
            Application.getCommonsCache().evict("ClassificationLEVEL-" + cid);
        }
    }
}
