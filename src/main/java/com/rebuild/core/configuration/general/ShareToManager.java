/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.configuration.general;

import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.Application;
import com.rebuild.core.configuration.ConfigManager;
import com.rebuild.core.privileges.UserHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public abstract class ShareToManager implements ConfigManager {

    
    public static final String SHARE_ALL = "ALL";
    
    public static final String SHARE_SELF = "SELF";

    
    abstract protected String getConfigEntity();

    
    protected String getConfigFields() {
        return "configId,shareTo,createdBy,config";
    }

    
    protected void cleanWithBelongEntity(ID cfgid, boolean hasApplyType) {
        String ql = String.format("select belongEntity%s from %s where configId = ?",
                (hasApplyType ? ",applyType" : ""), getConfigEntity());
        Object[] c = Application.createQueryNoFilter(ql).setParameter(1, cfgid).unique();
        if (c != null) {
            Application.getCommonsCache().evict(formatCacheKey((String) c[0], hasApplyType ? (String) c[1] : null));
        }
    }

    
    public ID detectUseConfig(ID user, String belongEntity, String applyType) {
        final Object[][] alls = getAllConfig(belongEntity, applyType);
        if (alls.length == 0) return null;

        
        for (Object[] d : alls) {
            ID createdBy = (ID) d[2];
            if (UserHelper.isSelf(user, createdBy)) {
                return (ID) d[0];
            }
        }

        
        for (Object[] d : alls) {
            if (isShareTo((String) d[1], user)) {
                return (ID) d[0];
            }
        }

        return null;
    }

    
    protected Object[][] getUsesConfig(ID user, String belongEntity, String applyType) {
        Object[][] cached = getAllConfig(belongEntity, applyType);
        List<Object[]> canUses = new ArrayList<>();
        for (Object[] d : cached) {
            ID createdBy = (ID) d[2];
            if (UserHelper.isSelf(user, createdBy) || isShareTo((String) d[1], user)) {
                canUses.add(d);
            }
        }
        return canUses.toArray(new Object[0][]);
    }

    
    protected Object[][] getAllConfig(String belongEntity, String applyType) {
        final String cacheKey = formatCacheKey(belongEntity, applyType);
        Object[][] cached = (Object[][]) Application.getCommonsCache().getx(cacheKey);

        if (cached == null) {
            List<String> sqlWhere = new ArrayList<>();
            if (belongEntity != null) {
                sqlWhere.add(String.format("belongEntity = '%s'", belongEntity));
            }
            if (applyType != null) {
                sqlWhere.add(String.format("applyType = '%s'", applyType));
            }

            String ql = String.format(
                    "select %s from %s where (1=1) order by modifiedOn desc", getConfigFields(), getConfigEntity());
            if (!sqlWhere.isEmpty()) {
                ql = ql.replace("(1=1)", StringUtils.join(sqlWhere.iterator(), " and "));
            }

            cached = Application.createQueryNoFilter(ql).array();
            Application.getCommonsCache().putx(cacheKey, cached);
        }

        if (cached == null) {
            return new Object[0][];
        }

        Object[][] clone = new Object[cached.length][];
        for (int i = 0; i < cached.length; i++) {
            clone[i] = (Object[]) ObjectUtils.clone(cached[i]);
        }
        return clone;
    }

    
    private boolean isShareTo(String shareTo, ID user) {
        if (SHARE_ALL.equals(shareTo)) {
            return true;

        } else if (shareTo != null && shareTo.length() >= 20) {
            Set<String> userDefs = new HashSet<>();
            CollectionUtils.addAll(userDefs, shareTo.split(","));
            Set<ID> sharedUsers = UserHelper.parseUsers(userDefs, null);
            return sharedUsers.contains(user);

        } else {  
            return false;
        }
    }

    
    final protected String formatCacheKey(String belongEntity, String applyType) {
        return String.format("%s-%s-%s.V6", getConfigEntity(),
                StringUtils.defaultIfBlank(belongEntity, "N"),
                StringUtils.defaultIfBlank(applyType, "N")).toUpperCase();
    }
}
