/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.support.general;

import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.Application;
import com.rebuild.core.metadata.MetadataHelper;
import com.rebuild.core.service.query.ParseHelper;
import com.rebuild.core.support.SetUser;

import java.util.HashSet;
import java.util.Set;


public class BatchOperatorQuery extends SetUser {

    
    public static final int DR_SELECTED = 1;
    
    public static final int DR_PAGED = 2;
    
    public static final int DR_QUERYED = 3;
    
    public static final int DR_ALL = 10;

    private int dataRange;
    private JSONObject queryData;

    
    public BatchOperatorQuery(int dataRange, JSONObject queryData) {
        this.dataRange = dataRange;
        this.queryData = queryData;
    }

    
    public JSONObject wrapQueryData(int maxRows, boolean clearFields) {
        if (this.dataRange != DR_PAGED) {
            queryData.put("pageNo", 1);
            queryData.put("pageSize", maxRows);  
        }
        if (this.dataRange == DR_SELECTED || this.dataRange == DR_ALL) {
            queryData.remove("filter");
            queryData.remove("advFilter");
        }
        if (this.dataRange == DR_SELECTED) {
            JSONObject idsItem = new JSONObject();
            idsItem.put("op", ParseHelper.IN);
            idsItem.put("field", getEntity().getPrimaryField().getName());
            idsItem.put("value", queryData.getString("_selected"));

            JSONArray items = new JSONArray();
            items.add(idsItem);
            JSONObject filter = new JSONObject();
            filter.put("items", items);
            queryData.put("filter", filter);
        }

        if (clearFields) {
            queryData.put("fields", new String[]{getEntity().getPrimaryField().getName()});
        }
        queryData.put("reload", Boolean.FALSE);
        return queryData;
    }

    
    protected String getFromClauseSql() {
        QueryParser queryParser = new QueryParser(wrapQueryData(Integer.MAX_VALUE, true));
        String fullSql = queryParser.toSql();
        return fullSql.substring(fullSql.indexOf(" from ")).trim();
    }

    
    public ID[] getQueryedRecords() {
        if (this.dataRange == DR_SELECTED) {
            String selected = queryData.getString("_selected");

            Set<ID> ids = new HashSet<>();
            for (String s : selected.split("\\|")) {
                if (ID.isId(s)) {
                    ids.add(ID.valueOf(s));
                }
            }
            return ids.toArray(new ID[0]);
        }

        String sql = String.format("select %s %s",
                getEntity().getPrimaryField().getName(), getFromClauseSql());
        int pageNo = queryData.getIntValue("pageNo");
        int pageSize = queryData.getIntValue("pageSize");

        Object[][] array = Application.createQuery(sql, getUser())
                .setLimit(pageSize, pageNo * pageSize - pageSize)
                .setTimeout(60)
                .array();

        Set<ID> ids = new HashSet<>();
        for (Object[] o : array) {
            ids.add((ID) o[0]);
        }
        return ids.toArray(new ID[0]);
    }

    private Entity getEntity() {
        String entityName = queryData.getString("entity");
        return MetadataHelper.getEntity(entityName);
    }
}
