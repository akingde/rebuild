/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.support;

import com.rebuild.core.support.i18n.LanguageBundle;


public enum ConfigurationItem {

    
    SN, DBVer, AppBuild,

    
    CacheHost, CachePort, CacheUser, CachePassword,

    
    AppName("REBUILD"),
    LOGO,
    LOGOWhite,
    HomeURL("https://getrebuild.com/"),
    PageFooter,

    
    StorageURL, StorageApiKey, StorageApiSecret, StorageBucket,

    
    MailUser, MailPassword, MailAddr, MailName(AppName), MailCc,
    MailSmtpServer,

    
    SmsUser, SmsPassword, SmsSign(AppName),

    
    OpenSignUp(true),

    
    LiveWallpaper(true),
    
    CustomWallpaper,

    
    FileSharable(true),

    
    MarkWatermark(false),

    
    PasswordPolicy(1),

    
    RevisionHistoryKeepingDays(180),

    
    RecycleBinKeepingDays(180),

    
    DBBackupsEnable(true),

    
    DBBackupsKeepingDays(180),

    
    MultipleSessions(true),

    
    DefaultLanguage(LanguageBundle.SYS_LC),

    
    ShowViewHistory(true),

    
    LoginCaptchaPolicy(1),

    
    PasswordExpiredDays(0),

    
    AllowUsesTime,
    
    AllowUsesIp,
    
    Login2FAMode(0),

    
    DingtalkAgentid, DingtalkAppkey, DingtalkAppsecret, DingtalkCorpid,
    DingtalkPushAeskey, DingtalkPushToken,
    DingtalkSyncUsers(false),
    DingtalkSyncUsersRole,
    
    WxworkCorpid, WxworkAgentid, WxworkSecret,
    WxworkRxToken, WxworkRxEncodingAESKey,
    WxworkAuthFile,
    WxworkSyncUsers(false),
    WxworkSyncUsersRole,
    
    SamlIdPEntityid, SamlIdPEndpoint, SamlIdPSloEndpoint, SamlIdPCert,

    
    PortalBaiduMapAk,
    PortalOfficePreviewUrl,
    PortalUploadMaxSize(100),

    
    DataDirectory,                  
    RedisDatabase(0),     
    MobileUrl,                      
    RbStoreUrl                      

    ;

    private Object defaultVal;

    ConfigurationItem() {
    }

    ConfigurationItem(Object defaultVal) {
        this.defaultVal = defaultVal;
    }

    
    public Object getDefaultValue() {
        if (defaultVal != null && defaultVal instanceof ConfigurationItem) {
            return ((ConfigurationItem) defaultVal).getDefaultValue();
        }
        return defaultVal;
    }
}
