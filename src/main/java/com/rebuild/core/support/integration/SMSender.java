/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.support.integration;

import cn.devezhao.commons.CalendarUtils;
import cn.devezhao.commons.ThreadPool;
import cn.devezhao.persist4j.Record;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.Application;
import com.rebuild.core.configuration.ConfigurationException;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.privileges.UserService;
import com.rebuild.core.support.ConfigurationItem;
import com.rebuild.core.support.HeavyStopWatcher;
import com.rebuild.core.support.License;
import com.rebuild.core.support.RebuildConfiguration;
import com.rebuild.core.support.i18n.Language;
import com.rebuild.utils.CommonsUtils;
import com.rebuild.utils.JSONUtils;
import com.rebuild.utils.MarkdownUtils;
import com.rebuild.utils.OkHttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@Slf4j
public class SMSender {

    private static final String STATUS_OK = "success";

    private static final int TYPE_SMS = 1;
    private static final int TYPE_EMAIL = 2;

    
    public static void sendMailAsync(String to, String subject, String content) {
        ThreadPool.exec(() -> {
            try {
                sendMail(to, subject, content);
            } catch (Exception ex) {
                log.error("Mail failed to send : " + to + " < " + subject, ex);
            }
        });
    }

    
    public static String sendMail(String to, String subject, String content) {
        return sendMail(to, subject, content, true, RebuildConfiguration.getMailAccount());
    }

    
    public static String sendMail(String to, String subject, String content, boolean useTemplate, String[] specAccount) throws ConfigurationException {
        if (specAccount == null || specAccount.length < 5
                || StringUtils.isBlank(specAccount[0]) || StringUtils.isBlank(specAccount[1])
                || StringUtils.isBlank(specAccount[2]) || StringUtils.isBlank(specAccount[3])) {
            throw new ConfigurationException(Language.L("邮件账户未配置或配置错误"));
        }

        
        if (useTemplate) {
            Element mailbody = getMailTemplate();

            Objects.requireNonNull(mailbody.selectFirst(".rb-title")).text(subject);
            Objects.requireNonNull(mailbody.selectFirst(".rb-content")).html(content);
            String htmlContent = mailbody.html();
            
            htmlContent = htmlContent.replace("%TO%", to);
            htmlContent = htmlContent.replace("%TIME%", CalendarUtils.getUTCDateTimeFormat().format(CalendarUtils.now()));
            htmlContent = htmlContent.replace("%APPNAME%", RebuildConfiguration.get(ConfigurationItem.AppName));

            String pageFooter = RebuildConfiguration.get(ConfigurationItem.PageFooter);
            if (StringUtils.isNotBlank(pageFooter)) {
                pageFooter = MarkdownUtils.render(pageFooter);
                htmlContent = htmlContent.replace("%PAGE_FOOTER%", pageFooter);
            } else {
                htmlContent = htmlContent.replace("%PAGE_FOOTER%", "");
            }

            content = htmlContent;
        }

        final String logContent = "【" + subject + "】" + content;

        
        if (specAccount.length >= 6 && StringUtils.isNotBlank(specAccount[5])) {
            try {
                String emailId = sendMailViaSmtp(to, subject, content, specAccount);
                createLog(to, logContent, TYPE_EMAIL, emailId, null);
                return emailId;

            } catch (EmailException ex) {
                log.error("SMTP failed to send : " + to + " > " + subject, ex);
                return null;
            }
        }

        Map<String, Object> params = new HashMap<>();
        params.put("appid", specAccount[0]);
        params.put("signature", specAccount[1]);
        params.put("to", to);
        if (StringUtils.isNotBlank(specAccount[4])) params.put("cc", specAccount[4]);
        params.put("from", specAccount[2]);
        params.put("from_name", specAccount[3]);
        params.put("subject", subject);
        if (useTemplate) {
            params.put("html", content);
        } else {
            params.put("text", content);
        }
        params.put("asynchronous", "true");
        params.put("headers", JSONUtils.toJSONObject("X-User-Agent", OkHttpUtils.RB_UA));

        JSONObject rJson;
        try {
            String r = OkHttpUtils.post("https://api-v4.mysubmail.com/mail/send.json", params);
            rJson = JSON.parseObject(r);
        } catch (Exception ex) {
            log.error("Submail failed to send : " + to + " > " + subject, ex);
            return null;
        }

        JSONArray returns = rJson.getJSONArray("return");
        if (STATUS_OK.equalsIgnoreCase(rJson.getString("status")) && !returns.isEmpty()) {
            String sendId = ((JSONObject) returns.get(0)).getString("send_id");
            createLog(to, logContent, TYPE_EMAIL, sendId, null);
            return sendId;

        } else {
            log.error("Mail failed to send : " + to + " > " + subject + "\nError : " + rJson);
            createLog(to, logContent, TYPE_EMAIL, null, rJson.getString("msg"));
            return null;
        }
    }

    
    protected static String sendMailViaSmtp(String to, String subject, String htmlContent, String[] specAccount) throws EmailException {
        HtmlEmail email = new HtmlEmail();
        email.addTo(to);
        if (StringUtils.isNotBlank(specAccount[4])) email.addCc(specAccount[4]);
        email.setSubject(subject);
        email.setHtmlMsg(htmlContent);

        email.setFrom(specAccount[2], specAccount[3]);
        email.setAuthentication(specAccount[0], specAccount[1]);

        
        String[] hostPortSsl = specAccount[5].split(":");
        email.setHostName(hostPortSsl[0]);
        if (hostPortSsl.length > 1) email.setSmtpPort(Integer.parseInt(hostPortSsl[1]));
        if (hostPortSsl.length > 2) email.setSSLOnConnect("ssl".equalsIgnoreCase(hostPortSsl[2]));

        email.addHeader("X-User-Agent", OkHttpUtils.RB_UA);
        email.setCharset("UTF-8");
        return email.send();
    }

    private static Element MT_CACHE = null;
    
    protected static Element getMailTemplate() {
        if (MT_CACHE != null && !Application.devMode()) return MT_CACHE.clone();

        String content = CommonsUtils.getStringOfRes("i18n/email.zh_CN.html");
        Assert.notNull(content, "Cannot load template of email");

        
        if (Application.isReady() && License.getCommercialType() > 10) {
            content = content.replace("REBUILD", RebuildConfiguration.get(ConfigurationItem.AppName));
            content = content.replace("https://getrebuild.com/img/logo.png", RebuildConfiguration.getHomeUrl("commons/theme/use-logo"));
            content = content.replace("https://getrebuild.com/", RebuildConfiguration.getHomeUrl());
        }

        Document html = Jsoup.parse(content);
        MT_CACHE = html.body();

        return MT_CACHE.clone();
    }

    
    public static void sendSMSAsync(String to, String content) {
        ThreadPool.exec(() -> {
            try {
                sendSMS(to, content);
            } catch (Exception ex) {
                log.error("SMS failed to send : " + to, ex);
            }
        });
    }

    
    public static String sendSMS(String to, String content) throws ConfigurationException {
        return sendSMS(to, content, RebuildConfiguration.getSmsAccount());
    }

    
    public static String sendSMS(String to, String content, String[] specAccount) throws ConfigurationException {
        if (specAccount == null || specAccount.length < 3
                || StringUtils.isBlank(specAccount[0]) || StringUtils.isBlank(specAccount[1])
                || StringUtils.isBlank(specAccount[2])) {
            throw new ConfigurationException(Language.L("短信账户未配置或配置错误"));
        }

        Map<String, Object> params = new HashMap<>();
        params.put("appid", specAccount[0]);
        params.put("signature", specAccount[1]);
        params.put("to", to);
        
        if (!content.startsWith("【")) {
            content = "【" + specAccount[2] + "】" + content;
        }
        params.put("content", content);

        HeavyStopWatcher.createWatcher("Subsms Send", to);
        JSONObject rJson;
        try {
            String r = OkHttpUtils.post("https://api-v4.mysubmail.com/sms/send.json", params);
            rJson = JSON.parseObject(r);
        } catch (Exception ex) {
            log.error("Subsms failed to send : " + to + " > " + content, ex);
            return null;
        } finally {
            HeavyStopWatcher.clean();
        }

        if (STATUS_OK.equalsIgnoreCase(rJson.getString("status"))) {
            String sendId = rJson.getString("send_id");
            createLog(to, content, TYPE_SMS, sendId, null);
            return sendId;

        } else {
            log.error("SMS failed to send : " + to + " > " + content + "\nError : " + rJson);
            createLog(to, content, TYPE_SMS, null, rJson.getString("msg"));
            return null;
        }
    }

    private static void createLog(String to, String content, int type, String sentid, String error) {
        if (!Application.isReady()) return;

        Record slog = EntityHelper.forNew(EntityHelper.SmsendLog, UserService.SYSTEM_USER);
        slog.setString("to", to);
        slog.setString("content", CommonsUtils.maxstr(content, 10000));
        slog.setDate("sendTime", CalendarUtils.now());
        slog.setInt("type", type);
        if (sentid != null) {
            slog.setString("sendResult", sentid);
        } else {
            slog.setString("sendResult",
                    CommonsUtils.maxstr("ERR:" + StringUtils.defaultIfBlank(error, "Unknow"), 200));
        }
        Application.getCommonsService().create(slog);
    }

    
    public static boolean availableSMS() {
        return RebuildConfiguration.getSmsAccount() != null;
    }

    
    public static boolean availableMail() {
        return RebuildConfiguration.getMailAccount() != null;
    }
}
