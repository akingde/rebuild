/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.support;

import cn.devezhao.persist4j.Record;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.Application;
import com.rebuild.core.BootEnvironmentPostProcessor;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.privileges.UserService;
import org.apache.commons.lang.StringUtils;


public class KVStorage {

    private static final Object SETNULL = new Object();

    
    public static String getCustomValue(String key) {
        return getValue("custom." + key, false, null);
    }

    
    public static void setCustomValue(String key, Object value) {
        setValue("custom." + key, value);
    }

    
    public static void removeCustomValue(String key) {
        setCustomValue(key, SETNULL);
    }

    
    protected static void setValue(final String key, Object value) {
        Object[] exists = Application.createQueryNoFilter(
                "select configId from SystemConfig where item = ?")
                .setParameter(1, key)
                .unique();

        
        if (value == SETNULL) {
            if (exists != null) {
                Application.getCommonsService().delete((ID) exists[0]);
                Application.getCommonsCache().evict(key);
            }
            return;
        }

        Record record;
        if (exists == null) {
            record = EntityHelper.forNew(EntityHelper.SystemConfig, UserService.SYSTEM_USER, false);
            record.setString("item", key);
        } else {
            record = EntityHelper.forUpdate((ID) exists[0], UserService.SYSTEM_USER, false);
        }
        record.setString("value", String.valueOf(value));

        Application.getCommonsService().createOrUpdate(record);
        Application.getCommonsCache().evict(key);
    }

    
    protected static String getValue(final String key, boolean noCache, Object defaultValue) {
        String value = null;

        if (Application.isReady()) {
            
            if (!noCache) {
                value = Application.getCommonsCache().get(key);
                if (value != null) {
                    return value;
                }
            }

            
            Object[] fromDb = Application.createQueryNoFilter(
                    "select value from SystemConfig where item = ?")
                    .setParameter(1, key)
                    .unique();
            value = fromDb == null ? null : StringUtils.defaultIfBlank((String) fromDb[0], null);
        }

        
        if (value == null) {
            value = BootEnvironmentPostProcessor.getProperty(key);
        }

        
        if (value == null && defaultValue != null) {
            value = defaultValue.toString();
        }

        if (Application.isReady()) {
            if (value == null) {
                Application.getCommonsCache().evict(key);
            } else {
                Application.getCommonsCache().put(key, value);
            }
        }

        return value;
    }
}
