/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.metadata.easymeta;

import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.Field;
import cn.devezhao.persist4j.dialect.FieldType;
import com.alibaba.fastjson.JSON;
import com.rebuild.core.metadata.MetadataHelper;
import com.rebuild.core.metadata.impl.EasyFieldConfigProps;
import com.rebuild.utils.JSONUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.util.Assert;


public abstract class EasyField extends BaseEasyMeta<Field> {
    private static final long serialVersionUID = 6027165766338449527L;

    private final DisplayType displayType;

    protected EasyField(Field field, DisplayType displayType) {
        super(field);
        this.displayType = displayType;
    }

    @Override
    public String getLabel() {
        return getRawMeta().getType() == FieldType.PRIMARY ? "ID" : super.getLabel();
    }

    @Override
    public boolean isBuiltin() {
        if (super.isBuiltin()) return true;

        Field field = getRawMeta();
        if (MetadataHelper.isCommonsField(field)) return true;

        if (field.getType() == FieldType.REFERENCE) {
            
            
            Entity hasMain = field.getOwnEntity().getMainEntity();
            return hasMain != null && hasMain.equals(field.getReferenceEntity()) && !field.isCreatable();
        }
        return false;
    }

    
    public String getDisplayType(boolean fullName) {
        DisplayType dt = getDisplayType();
        if (fullName) {
            return dt.getDisplayName() + " (" + dt.name() + ")";
        } else {
            return dt.name();
        }
    }

    
    public DisplayType getDisplayType() {
        return displayType;
    }

    @Override
    public JSON toJSON() {
        return JSONUtils.toJSONObject(
                new String[] { "name", "label", "type", "nullable", "creatable", "updatable" },
                new Object[] { getName(), getLabel(), getDisplayType().name(),
                        getRawMeta().isNullable(), getRawMeta().isCreatable(), getRawMeta().isUpdatable() });
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "#" + getRawMeta().toString();
    }

    
    public boolean isNullable() {
        return getRawMeta().isNullable();
    }

    
    public boolean isRepeatable() {
        return getRawMeta().isRepeatable();
    }

    

    
    public Object convertCompatibleValue(Object value, EasyField targetField) {
        DisplayType targetType = targetField.getDisplayType();
        boolean is2Text = targetType == DisplayType.TEXT || targetType == DisplayType.NTEXT;
        if (is2Text) {
            Object wrappedValue = wrapValue(value);
            if (wrappedValue == null) return null;
            return StringUtils.defaultIfBlank(wrappedValue.toString(), null);
        }

        Assert.isTrue(targetField.getDisplayType() == getDisplayType(), "type-by-type is must");
        return value;
    }

    
    public Object exprDefaultValue() {
        Object dv = getRawMeta().getDefaultValue();
        if (dv == null) return null;
        return StringUtils.defaultIfBlank(dv.toString(), null);
    }

    
    public Object wrapValue(Object value) {
        if (value == null) return null;
        if (value instanceof String) return value.toString().trim();
        return value;
    }








    
    public boolean isDesensitized() {
        return BooleanUtils.toBoolean(getExtraAttr(EasyFieldConfigProps.ADV_DESENSITIZED));
    }
}
