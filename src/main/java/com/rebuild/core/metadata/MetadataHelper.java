/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.metadata;

import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.Field;
import cn.devezhao.persist4j.dialect.FieldType;
import cn.devezhao.persist4j.engine.ID;
import cn.devezhao.persist4j.metadata.MetadataException;
import cn.devezhao.persist4j.metadata.MissingMetaExcetion;
import cn.devezhao.persist4j.query.compiler.QueryCompiler;
import com.rebuild.core.Application;
import com.rebuild.core.metadata.easymeta.EasyMetaFactory;
import com.rebuild.core.metadata.impl.DynamicMetadataFactory;
import com.rebuild.core.metadata.impl.EasyEntityConfigProps;
import com.rebuild.core.metadata.impl.GhostEntity;
import com.rebuild.core.support.i18n.Language;
import com.rebuild.utils.CommonsUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import java.util.*;


@Slf4j
public class MetadataHelper {

    
    public static final String SPLITER = "$$$$";
    public static final String SPLITER_RE = "\\$\\$\\$\\$";

    
    public static DynamicMetadataFactory getMetadataFactory() {
        return (DynamicMetadataFactory) Application.getPersistManagerFactory().getMetadataFactory();
    }

    
    public static Entity[] getEntities() {
        return getMetadataFactory().getEntities();
    }

    
    public static boolean containsEntity(String entityName) {
        try {
            return !(getEntity(entityName) instanceof GhostEntity);
        } catch (MissingMetaExcetion ex) {
            return false;
        }
    }

    
    public static boolean containsEntity(int entityCode) {
        try {
            getEntity(entityCode);
            return true;
        } catch (MissingMetaExcetion ex) {
            return false;
        }
    }

    
    public static boolean containsField(String entityName, String fieldName) {
        try {
            return getEntity(entityName).containsField(fieldName);
        } catch (MissingMetaExcetion ex) {
            return false;
        }
    }

    
    public static Entity getEntity(String entityName) throws MissingMetaExcetion {
        try {
            return getMetadataFactory().getEntity(entityName);
        } catch (MissingMetaExcetion ex) {
            throw new MissingMetaExcetion(Language.L("实体 [%s] 已经不存在，请检查配置", entityName.toUpperCase()));
        }
    }

    
    public static Entity getEntity(int entityCode) throws MissingMetaExcetion {
        try {
            return getMetadataFactory().getEntity(entityCode);
        } catch (MissingMetaExcetion ex) {
            throw new MissingMetaExcetion(Language.L("实体 [%s] 已经不存在，请检查配置", entityCode));
        }
    }

    
    public static String getEntityName(ID record) {
        return getEntity(record.getEntityCode()).getName();
    }

    
    public static Field getField(String entityName, String fieldName) throws MissingMetaExcetion {
        try {
            return getEntity(entityName).getField(fieldName);
        } catch (MissingMetaExcetion ex) {
            throw new MissingMetaExcetion(
                    Language.L("字段 [%s] 已经不存在，请检查配置", (entityName + "#" + fieldName).toUpperCase()));
        }
    }

    
    public static Field[] getReferenceToFields(Entity source, Entity reference) {
        List<Field> fields = new ArrayList<>();
        for (Field field : reference.getFields()) {
            if (field.getType() == FieldType.REFERENCE && field.getReferenceEntity().equals(source)) {
                fields.add(field);
            }
        }
        return fields.toArray(new Field[0]);
    }

    
    public static Field[] getReferenceToFields(Entity source) {
        List<Field> fields = new ArrayList<>();
        for (Entity entity : getEntities()) {
            CollectionUtils.addAll(fields, getReferenceToFields(source, entity));
        }
        return fields.toArray(new Field[0]);
    }

    
    public static boolean isSystemField(Field field) {
        return isSystemField(field.getName()) || field.getType() == FieldType.PRIMARY;
    }

    
    public static boolean isSystemField(String fieldName) {
        return EntityHelper.AutoId.equalsIgnoreCase(fieldName)
                || EntityHelper.QuickCode.equalsIgnoreCase(fieldName)
                || EntityHelper.IsDeleted.equalsIgnoreCase(fieldName)
                || EntityHelper.ApprovalStepNode.equalsIgnoreCase(fieldName);
    }

    
    public static boolean isCommonsField(Field field) {
        return isSystemField(field) || isCommonsField(field.getName());
    }

    
    public static boolean isCommonsField(String fieldName) {
        if (isSystemField(fieldName) || isApprovalField(fieldName)) return true;

        return EntityHelper.OwningUser.equalsIgnoreCase(fieldName) || EntityHelper.OwningDept.equalsIgnoreCase(fieldName)
                || EntityHelper.CreatedOn.equalsIgnoreCase(fieldName) || EntityHelper.CreatedBy.equalsIgnoreCase(fieldName)
                || EntityHelper.ModifiedOn.equalsIgnoreCase(fieldName) || EntityHelper.ModifiedBy.equalsIgnoreCase(fieldName);
    }

    
    public static boolean isApprovalField(String fieldName) {
        return EntityHelper.ApprovalId.equalsIgnoreCase(fieldName)
                || EntityHelper.ApprovalState.equalsIgnoreCase(fieldName)
                || EntityHelper.ApprovalStepNode.equalsIgnoreCase(fieldName)
                || EntityHelper.ApprovalLastUser.equalsIgnoreCase(fieldName);
    }

    
    public static boolean isBizzEntity(int entityCode) {
        return entityCode == EntityHelper.User || entityCode == EntityHelper.Department
                || entityCode == EntityHelper.Role || entityCode == EntityHelper.Team;
    }

    
    public static boolean isBizzEntity(Entity entity) {
        return isBizzEntity(entity.getEntityCode());
    }

    
    public static boolean isBusinessEntity(Entity entity) {
        if (entity.getMainEntity() != null) entity = entity.getMainEntity();
        return hasPrivilegesField(entity) || EasyMetaFactory.valueOf(entity).isPlainEntity();
    }

    
    public static boolean hasPrivilegesField(Entity entity) {
        return entity.containsField(EntityHelper.OwningUser) && entity.containsField(EntityHelper.OwningDept);
    }

    
    public static boolean hasApprovalField(Entity entity) {
        return entity.containsField(EntityHelper.ApprovalId) && entity.containsField(EntityHelper.ApprovalState);
    }

    
    public static Field getDetailToMainField(Entity detail) {
        Entity main = detail.getMainEntity();
        Assert.isTrue(main != null, "None detail-entity");

        for (Field field : detail.getFields()) {
            if (field.getType() != FieldType.REFERENCE) {
                continue;
            }
            
            if (main.equals(field.getReferenceEntity()) && !field.isCreatable()) {
                return field;
            }
        }
        throw new MetadataException("Bad detail-entity (No DTM)");
    }

    
    public static Field getLastJoinField(Entity entity, String fieldPath) {
        String[] paths = fieldPath.split("\\.");
        if (fieldPath.charAt(0) == QueryCompiler.NAME_FIELD_PREFIX) {
            paths[0] = paths[0].substring(1);
            if (!entity.containsField(paths[0])) {
                return null;
            }
        }

        Field lastField = null;
        Entity father = entity;
        for (String field : paths) {
            if (father != null && father.containsField(field)) {
                lastField = father.getField(field);
                if (lastField.getType() == FieldType.REFERENCE) {
                    father = lastField.getReferenceEntity();
                } else {
                    father = null;
                }
            } else {
                return null;
            }
        }
        return lastField;
    }

    
    public static boolean checkAndWarnField(Entity entity, String fieldName) {
        if (entity.containsField(fieldName)) return true;
        log.warn("Unknown field `{}` in `{}`", fieldName, entity.getName());
        CommonsUtils.printStackTrace();
        return false;
    }

    
    public static boolean checkAndWarnField(String entityName, String fieldName) {
        if (!containsEntity(entityName)) {
            return false;
        }
        return checkAndWarnField(getEntity(entityName), fieldName);
    }

    
    public static Set<String> getEntityTags() {
        Set<String> set = new TreeSet<>();
        for (Entity entity : getEntities()) {
            String tags = EasyMetaFactory.valueOf(entity).getExtraAttr(EasyEntityConfigProps.TAGS);
            if (StringUtils.isNotBlank(tags)) {
                Collections.addAll(set, tags.split(","));
            }
        }
        return set;
    }
}
