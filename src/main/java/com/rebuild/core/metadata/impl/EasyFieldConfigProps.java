/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.metadata.impl;


public class EasyFieldConfigProps {

    
    public static final String NUMBER_NOTNEGATIVE = "notNegative";
    
    public static final String NUMBER_FORMAT = "numberFormat";
    
    public static final String NUMBER_CALCFORMULA = "calcFormula";

    
    public static final String DECIMAL_NOTNEGATIVE = NUMBER_NOTNEGATIVE;
    
    public static final String DECIMAL_FORMAT = "decimalFormat";
    
    public static final String DECIMAL_CALCFORMULA = NUMBER_CALCFORMULA;

    
    public static final String DATE_FORMAT = "dateFormat";

    
    public static final String DATETIME_FORMAT = "datetimeFormat";

    
    public static final String TIME_FORMAT = "timeFormat";

    
    public static final String FILE_UPLOADNUMBER = "uploadNumber";

    
    public static final String IMAGE_UPLOADNUMBER = FILE_UPLOADNUMBER;

    
    public static final String SERIES_FORMAT = "seriesFormat";
    
    public static final String SERIES_ZERO = "seriesZero";

    
    public static final String CLASSIFICATION_USE = "classification";

    
    public static final String CLASSIFICATION_LEVEL = "classificationLevel";

    
    public static final String STATE_CLASS = "stateClass";

    
    public static final String REFERENCE_DATAFILTER = "referenceDataFilter";
    
    public static final String REFERENCE_CASCADINGFIELD = "referenceCascadingField";

    
    public static final String N2NREFERENCE_DATAFILTER = REFERENCE_DATAFILTER;
    
    public static final String N2NREFERENCE_CASCADINGFIELD = REFERENCE_CASCADINGFIELD;

    
    public static final String NTEXT_USEMDEDIT = "useMdedit";

    

    public static final String ADV_DESENSITIZED = "advDesensitized";

    
    public static final String ADV_PATTERN = "advPattern";

    
    public static final String LOCATION_MAPONVIEW = "locationMapOnView";
    
    public static final String LOCATION_AUTOLOCATION = "locationAutoLocation";

    
    public static final String TEXT_SCANCODE = "textScanCode";
}
