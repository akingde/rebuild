/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.metadata.impl;


public class EasyEntityConfigProps {

    
    public static final String QUICK_FIELDS = "quickFields";
    
    public static final String TAGS = "tags";

    
    public static final String ADV_LIST_HIDE_FILTERS = "advListHideFilters";
    
    public static final String ADV_LIST_HIDE_CHARTS = "advListHideCharts";
    
    public static final String ADV_LIST_MODE = "advListMode";
}
