/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.rbstore;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.Feature;
import com.rebuild.core.BootEnvironmentPostProcessor;
import com.rebuild.core.RebuildException;
import com.rebuild.core.support.ConfigurationItem;
import com.rebuild.utils.JSONUtils;
import com.rebuild.utils.OkHttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;


@Slf4j
public class RBStore {

    
    private static final String DATA_REPO = BootEnvironmentPostProcessor.getProperty(
            ConfigurationItem.RbStoreUrl.name(), "https://getrebuild.com/gh/getrebuild/rebuild-datas/");

    
    public static JSON fetchClassification(String fileUri) {
        return fetchRemoteJson("classifications/" + fileUri);
    }

    
    public static JSON fetchMetaschema(String fileUri) {
        return fetchRemoteJson("metaschemas/" +
                StringUtils.defaultIfBlank(fileUri, "index-3.0.json"));
    }

    
    public static JSON fetchRemoteJson(String fileUrl) throws RebuildException {
        if (!fileUrl.startsWith("http")) {
            fileUrl = DATA_REPO + (fileUrl.startsWith("/") ? fileUrl.substring(1) : fileUrl);
        }

        try {
            String content = OkHttpUtils.get(fileUrl);
            if (JSONUtils.wellFormat(content)) {
                return (JSON) JSON.parse(content, Feature.OrderedField);
            }

        } catch (Exception e) {
            log.error("Unable to read data from URL : {}", fileUrl, e);
        }

        throw new RebuildException("Unable to read data from RB-Store");
    }
}
