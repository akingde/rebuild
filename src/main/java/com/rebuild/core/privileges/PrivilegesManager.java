/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.privileges;

import cn.devezhao.bizz.BizzException;
import cn.devezhao.bizz.privileges.DepthEntry;
import cn.devezhao.bizz.privileges.Permission;
import cn.devezhao.bizz.privileges.Privileges;
import cn.devezhao.bizz.privileges.PrivilegesException;
import cn.devezhao.bizz.privileges.impl.BizzDepthEntry;
import cn.devezhao.bizz.privileges.impl.BizzPermission;
import cn.devezhao.bizz.security.member.Role;
import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.Field;
import cn.devezhao.persist4j.Filter;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.Application;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.metadata.MetadataHelper;
import com.rebuild.core.metadata.easymeta.EasyMetaFactory;
import com.rebuild.core.privileges.bizz.*;
import com.rebuild.core.service.NoRecordFoundException;
import com.rebuild.core.service.general.EntityService;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;


@Service
public class PrivilegesManager {

    final private UserStore theUserStore;
    final private RecordOwningCache theRecordOwningCache;

    
    protected PrivilegesManager(UserStore us, RecordOwningCache roc) {
        this.theUserStore = us;
        this.theRecordOwningCache = roc;
    }

    
    public Privileges getPrivileges(ID user, int entity) {
        User u = theUserStore.getUser(user);
        if (!u.isActive()) {
            return Privileges.NONE;
        } else if (u.isAdmin()) {
            return Privileges.ROOT;
        }
        return u.getOwningRole().getPrivileges(convert2MainEntity(entity));
    }

    
    public boolean allowCreate(ID user, int entity) {
        return allow(user, entity, BizzPermission.CREATE);
    }

    
    public boolean allowDelete(ID user, int entity) {
        return allow(user, entity, BizzPermission.DELETE);
    }

    
    public boolean allowUpdate(ID user, int entity) {
        return allow(user, entity, BizzPermission.UPDATE);
    }

    
    public boolean allowRead(ID user, int entity) {
        return allow(user, entity, BizzPermission.READ);
    }

    
    public boolean allowAssign(ID user, int entity) {
        return allow(user, entity, BizzPermission.ASSIGN);
    }

    
    public boolean allowShare(ID user, int entity) {
        return allow(user, entity, BizzPermission.SHARE);
    }

    
    public boolean allowDelete(ID user, ID target) {
        return allow(user, target, BizzPermission.DELETE);
    }

    
    public boolean allowUpdate(ID user, ID target) {
        return allow(user, target, BizzPermission.UPDATE);
    }

    
    public boolean allowRead(ID user, ID target) {
        return allow(user, target, BizzPermission.READ);
    }

    
    public boolean allowAssign(ID user, ID target) {
        return allow(user, target, BizzPermission.ASSIGN);
    }

    
    public boolean allowShare(ID user, ID target) {
        return allow(user, target, BizzPermission.SHARE);
    }

    
    public boolean allow(ID user, int entity, Permission action) {
        
        if (action.getMask() <= BizzPermission.READ.getMask() && EasyMetaFactory.valueOf(entity).isPlainEntity()) {
            return true;
        }

        

        if ((entity == EntityHelper.Feeds || entity == EntityHelper.ProjectTask)
                && (action == BizzPermission.READ || action == BizzPermission.CREATE)) {
            return true;
        } else if (entity == EntityHelper.Attachment && action == BizzPermission.READ) {
            return true;
        }

        Boolean a = userAllow(user);
        if (a != null) {
            return a;
        }

        Role role = theUserStore.getUser(user).getOwningRole();
        if (RoleService.ADMIN_ROLE.equals(role.getIdentity())) {
            return true;
        } else if (action == BizzPermission.READ && MetadataHelper.isBizzEntity(entity)) {
            return true;
        }

        
        if (action == EntityService.UNSHARE) {
            action = BizzPermission.SHARE;
        }

        if (MetadataHelper.getEntity(entity).getMainEntity() != null) {
            
            
            if (action == BizzPermission.CREATE) {
                throw new PrivilegesException("Unsupported checks detail-entity : " + entity);
            }
            
            else if (action == BizzPermission.ASSIGN || action == BizzPermission.SHARE) {
                return false;
            }
            action = convert2MainAction(action);
        }

        Privileges ep = role.getPrivileges(convert2MainEntity(entity));
        return ep.allowed(action);
    }

    
    public boolean allow(ID user, ID target, Permission action) {
        return allow(user, target, action, false);
    }

    
    public boolean allow(ID user, ID target, Permission action, boolean ignoreShared) {
        final Entity entity = MetadataHelper.getEntity(target.getEntityCode());

        
        if (action.getMask() <= BizzPermission.READ.getMask()
                && EasyMetaFactory.valueOf(entity).isPlainEntity()) {
            return true;
        }

        Boolean a = userAllow(user);
        if (a != null) {
            return a;
        }

        Role role = theUserStore.getUser(user).getOwningRole();
        if (RoleService.ADMIN_ROLE.equals(role.getIdentity())) {
            return true;
        }

        
        if (action == BizzPermission.READ && MetadataHelper.isBizzEntity(entity.getEntityCode())) {
            return true;
        }

        
        if (action == BizzPermission.UPDATE && target.equals(user)) {
            return true;
        }

        
        if (action == EntityService.UNSHARE) {
            action = BizzPermission.SHARE;
        }

        
        if (entity.getMainEntity() != null) {
            if (action == BizzPermission.ASSIGN || action == BizzPermission.SHARE) {
                return false;
            }
            action = convert2MainAction(action);
        }

        Privileges ep = role.getPrivileges(convert2MainEntity(entity.getEntityCode()));

        boolean allowed = ep.allowed(action);
        if (!allowed) {
            return false;
        }

        final DepthEntry depth = ep.superlative(action);

        if (BizzDepthEntry.NONE.equals(depth)) {
            return false;
        } else if (BizzDepthEntry.GLOBAL.equals(depth)) {
            return andPassCustomFilter(user, target, action, ep);
        }

        ID targetUserId = theRecordOwningCache.getOwningUser(target);
        if (targetUserId == null) {
            return false;
        }

        if (BizzDepthEntry.PRIVATE.equals(depth)) {
            allowed = user.equals(targetUserId);
            if (!allowed) {
                allowed = !ignoreShared && allowViaShare(user, target, action);
            }
            return allowed && andPassCustomFilter(user, target, action, ep);
        }

        User accessUser = theUserStore.getUser(user);
        User targetUser = theUserStore.getUser(targetUserId);
        Department accessUserDept = accessUser.getOwningDept();

        if (BizzDepthEntry.LOCAL.equals(depth)) {
            allowed = accessUserDept.equals(targetUser.getOwningDept());
            if (!allowed) {
                allowed = !ignoreShared && allowViaShare(user, target, action);
            }
            return allowed && andPassCustomFilter(user, target, action, ep);

        } else if (BizzDepthEntry.DEEPDOWN.equals(depth)) {
            if (accessUserDept.equals(targetUser.getOwningDept())) {
                return andPassCustomFilter(user, target, action, ep);
            }

            allowed = accessUserDept.isChildren(targetUser.getOwningDept(), true);
            if (!allowed) {
                allowed = !ignoreShared && allowViaShare(user, target, action);
            }
            return allowed && andPassCustomFilter(user, target, action, ep);
        }

        return false;
    }

    
    public boolean allowViaShare(ID user, ID target, Permission action) {
        if (!(action == BizzPermission.READ || action == BizzPermission.UPDATE)) {
            return false;
        }

        

        Entity entity = MetadataHelper.getEntity(target.getEntityCode());
        if (entity.getMainEntity() != null) {
            ID mainId = getMainRecordId(target);
            if (mainId == null) {
                throw new NoRecordFoundException("No record found by detail-id : " + target);
            }

            target = mainId;
            entity = entity.getMainEntity();
        }

        Object[] rights = Application.createQueryNoFilter(
                "select rights from ShareAccess where belongEntity = ? and recordId = ? and shareTo = ?")
                .setParameter(1, entity.getName())
                .setParameter(2, target)
                .setParameter(3, user)
                .unique();
        int rightsMask = rights == null ? 0 : (int) rights[0];
        return (rightsMask & action.getMask()) != 0;
    }

    
    private boolean andPassCustomFilter(ID user, ID target, Permission action, Privileges ep) {
        if (!(ep instanceof CustomEntityPrivileges)) return true;
        if (((CustomEntityPrivileges) ep).getCustomFilter(action) == null) return true;

        

        Entity entity = MetadataHelper.getEntity(target.getEntityCode());
        Filter customFilter = createQueryFilter(user, action);

        String sql = MessageFormat.format("select {0} from {1} where {0} = ''{2}''",
                entity.getPrimaryField().getName(), entity.getName(), target);

        Object hasOne = Application.getQueryFactory().createQuery(sql, customFilter).unique();
        return hasOne != null;
    }

    
    private int convert2MainEntity(int entity) {
        Entity em = MetadataHelper.getEntity(entity);
        return em.getMainEntity() == null ? entity : em.getMainEntity().getEntityCode();
    }

    
    private Permission convert2MainAction(Permission detailAction) {
        if (detailAction == BizzPermission.CREATE || detailAction == BizzPermission.DELETE) {
            return BizzPermission.UPDATE;
        }
        return detailAction;
    }

    
    private ID getMainRecordId(ID detailId) {
        Entity entity = MetadataHelper.getEntity(detailId.getEntityCode());
        Field dtmField = MetadataHelper.getDetailToMainField(entity);

        Object[] primary = Application.getQueryFactory().uniqueNoFilter(detailId, dtmField.getName());
        return primary == null ? null : (ID) primary[0];
    }

    
    private Boolean userAllow(ID user) {
        if (UserHelper.isAdmin(user)) return Boolean.TRUE;
        if (!theUserStore.getUser(user).isActive()) return Boolean.FALSE;
        return null;
    }

    
    public boolean allow(ID user, ZeroEntry entry) {
        Boolean a = userAllow(user);
        if (a != null) {
            return a;
        }

        Role role = theUserStore.getUser(user).getOwningRole();
        if (RoleService.ADMIN_ROLE.equals(role.getIdentity())) {
            return true;
        }

        if (role.hasPrivileges(entry.name())) {
            return role.getPrivileges(entry.name()).allowed(ZeroPermission.ZERO);
        }
        return entry.getDefaultVal();
    }

    
    public Filter createQueryFilter(ID user) {
        return createQueryFilter(user, BizzPermission.READ);
    }

    
    public Filter createQueryFilter(ID user, Permission action) {
        User theUser = theUserStore.getUser(user);
        if (theUser.isAdmin()) {
            return RoleBaseQueryFilter.ALLOWED;
        }
        return new RoleBaseQueryFilter(theUser, action);
    }

    

    
    private static final Permission[] RB_PERMISSIONS = new Permission[] {
            BizzPermission.CREATE,
            BizzPermission.DELETE,
            BizzPermission.UPDATE,
            BizzPermission.READ,
            BizzPermission.ASSIGN,
            BizzPermission.SHARE,
            EntityService.UNSHARE
    };

    
    public static Permission parse(String name) {
        for (Permission p : RB_PERMISSIONS) {
            if (p.getName().equalsIgnoreCase(name)) {
                return p;
            }
        }
        throw new BizzException("Unknown Permission : " + name);
    }
}
