/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.query;

import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.Application;
import com.rebuild.core.metadata.MetadataHelper;


public class FilterRecordChecker {

    private JSONObject filterExp;

    public FilterRecordChecker(JSONObject filterExp) {
        this.filterExp = filterExp;
    }

    
    public boolean check(ID recordId) {
        if (filterExp == null || filterExp.isEmpty()
                || filterExp.getJSONArray("items") == null || filterExp.getJSONArray("items").isEmpty()) {
            return true;
        }

        Entity entity = MetadataHelper.getEntity(recordId.getEntityCode());

        String sqlWhere = new AdvFilterParser(entity, filterExp)
                .toSqlWhere();
        sqlWhere += String.format(" and (%s = '%s')", entity.getPrimaryField().getName(), recordId);

        String checkSql = String.format("select %s from %s where %s",
                entity.getPrimaryField().getName(), entity.getName(), sqlWhere);

        return Application.createQueryNoFilter(checkSql).unique() != null;
    }
}
