/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.query;

import cn.devezhao.commons.CalendarUtils;
import cn.devezhao.commons.ObjectUtils;
import cn.devezhao.momentjava.Moment;
import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.Field;
import cn.devezhao.persist4j.dialect.FieldType;
import cn.devezhao.persist4j.dialect.Type;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.Application;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.metadata.MetadataHelper;
import com.rebuild.core.metadata.easymeta.DisplayType;
import com.rebuild.core.metadata.easymeta.EasyMetaFactory;
import com.rebuild.core.privileges.UserHelper;
import com.rebuild.core.privileges.bizz.Department;
import com.rebuild.core.support.SetUser;
import com.rebuild.core.support.i18n.Language;
import com.rebuild.utils.JSONUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.util.Assert;

import java.util.*;

import static cn.devezhao.commons.CalendarUtils.addDay;
import static cn.devezhao.commons.CalendarUtils.addMonth;


@Slf4j
public class AdvFilterParser extends SetUser {

    
    private static final String MODE_QUICK = "QUICK";

    private JSONObject filterExp;
    private Entity rootEntity;

    private Set<String> includeFields = null;

    
    public AdvFilterParser(JSONObject filterExp) {
        this(MetadataHelper.getEntity(filterExp.getString("entity")), filterExp);
    }

    
    public AdvFilterParser(Entity rootEntity, JSONObject filterExp) {
        this.rootEntity = rootEntity;
        this.filterExp = filterExp;
    }

    
    public String toSqlWhere() {
        if (filterExp == null || filterExp.isEmpty()) {
            return null;
        }

        this.includeFields = new HashSet<>();

        
        if (MODE_QUICK.equalsIgnoreCase(filterExp.getString("type"))) {
            JSONArray quickItems = buildQuickFilterItems(filterExp.getString("quickFields"));
            this.filterExp.put("items", quickItems);
        }

        JSONArray items = filterExp.getJSONArray("items");
        items = items == null ? JSONUtils.EMPTY_ARRAY : items;

        JSONObject values = filterExp.getJSONObject("values");
        values = values == null ? JSONUtils.EMPTY_OBJECT : values;

        String equation = StringUtils.defaultIfBlank(filterExp.getString("equation"), "OR");

        Map<Integer, String> indexItemSqls = new LinkedHashMap<>();
        int incrIndex = 1;
        for (Object o : items) {
            JSONObject item = (JSONObject) o;
            Integer index = item.getInteger("index");
            if (index == null) {
                index = incrIndex++;
            }

            String itemSql = parseItem(item, values);
            if (itemSql != null) {
                indexItemSqls.put(index, itemSql.trim());
                this.includeFields.add(item.getString("field"));
            }
            if (Application.devMode()) log.info("[dev] Parse item : {} >> {}", item, itemSql);
        }

        if (indexItemSqls.isEmpty()) return null;

        String equationHold = equation;
        if ((equation = validEquation(equation)) == null) {
            throw new FilterParseException(Language.L("无效的高级表达式 : %s", equationHold));
        }

        if ("OR".equalsIgnoreCase(equation)) {
            return "( " + StringUtils.join(indexItemSqls.values(), " or ") + " )";
        } else if ("AND".equalsIgnoreCase(equation)) {
            return "( " + StringUtils.join(indexItemSqls.values(), " and ") + " )";
        } else {
            
            String[] tokens = equation.toLowerCase().split(" ");
            List<String> itemSqls = new ArrayList<>();
            for (String token : tokens) {
                if (StringUtils.isBlank(token)) {
                    continue;
                }

                boolean hasRP = false;  
                if (token.length() > 1) {
                    if (token.startsWith("(")) {
                        itemSqls.add("(");
                        token = token.substring(1);
                    } else if (token.endsWith(")")) {
                        hasRP = true;
                        token = token.substring(0, token.length() - 1);
                    }
                }

                if (NumberUtils.isDigits(token)) {
                    String itemSql = StringUtils.defaultIfBlank(indexItemSqls.get(Integer.valueOf(token)), "(9=9)");
                    itemSqls.add(itemSql);
                } else if ("(".equals(token) || ")".equals(token) || "or".equals(token) || "and".equals(token)) {
                    itemSqls.add(token);
                } else {
                    log.warn("Invalid equation token : " + token);
                }

                if (hasRP) {
                    itemSqls.add(")");
                }
            }
            return "( " + StringUtils.join(itemSqls, " ") + " )";
        }
    }

    
    public Set<String> getIncludeFields() {
        Assert.notNull(includeFields, "Calls #toSqlWhere first");
        return includeFields;
    }

    
    private String parseItem(JSONObject item, JSONObject values) {
        String field = item.getString("field");
        if (field.startsWith("&amp;")) field = field.replace("&amp;", "&");  

        boolean hasNameFlag = field.startsWith("&");
        if (hasNameFlag) {
            field = field.substring(1);
        }

        Field fieldMeta = MetadataHelper.getLastJoinField(rootEntity, field);
        if (fieldMeta == null) {
            log.warn("Unknown field '{}' in '{}'", field, rootEntity.getName());
            return null;
        }

        DisplayType dt = EasyMetaFactory.getDisplayType(fieldMeta);
        if (dt == DisplayType.CLASSIFICATION
                || (dt == DisplayType.PICKLIST && hasNameFlag) ) {
            field = "&" + field;
        } else if (hasNameFlag) {
            if (!(dt == DisplayType.REFERENCE || dt == DisplayType.N2NREFERENCE)) {
                log.warn("Non reference-field '{}' in '{}'", field, rootEntity.getName());
                return null;
            }

            
            if (dt == DisplayType.REFERENCE) {
                fieldMeta = fieldMeta.getReferenceEntity().getNameField();
                dt = EasyMetaFactory.getDisplayType(fieldMeta);
                field += "." + fieldMeta.getName();
            }
        }

        String op = item.getString("op");
        String value = item.getString("value");
        String valueEnd = null;

        
        
        if (hasNameFlag && dt == DisplayType.N2NREFERENCE) {
            Entity refEntity = fieldMeta.getReferenceEntity();
            String inWhere = String.format("select %s from %s where %s %s %s",
                    refEntity.getPrimaryField().getName(),
                    refEntity.getName(),
                    refEntity.getNameField().getName(),
                    ParseHelper.convetOperation(op),
                    quoteValue('%' + value + '%', FieldType.STRING));

            return String.format(
                    "exists (select recordId from NreferenceItem where ^%s = recordId and belongField = '%s' and referenceId in (%s))",
                    rootEntity.getPrimaryField().getName(), fieldMeta.getName(), inWhere);
        }

        

        final boolean isDatetime = dt == DisplayType.DATETIME;

        
        if (isDatetime || dt == DisplayType.DATE) {

            final boolean isREX = ParseHelper.RED.equalsIgnoreCase(op)
                    || ParseHelper.REM.equalsIgnoreCase(op)
                    || ParseHelper.REY.equalsIgnoreCase(op);

            if (ParseHelper.TDA.equalsIgnoreCase(op)
                    || ParseHelper.YTA.equalsIgnoreCase(op)
                    || ParseHelper.TTA.equalsIgnoreCase(op)) {

                if (ParseHelper.YTA.equalsIgnoreCase(op)) {
                    value = formatDate(addDay(-1), 0);
                } else if (ParseHelper.TTA.equalsIgnoreCase(op)) {
                    value = formatDate(addDay(1), 0);
                } else {
                    value = formatDate(CalendarUtils.now(), 0);
                }

                if (isDatetime) {
                    op = ParseHelper.BW;
                    valueEnd = parseValue(value, op, fieldMeta, true);
                }

            } else if (ParseHelper.CUW.equalsIgnoreCase(op)
                    || ParseHelper.CUM.equalsIgnoreCase(op)
                    || ParseHelper.CUQ.equalsIgnoreCase(op)
                    || ParseHelper.CUY.equalsIgnoreCase(op)) {

                Date begin = Moment.moment().startOf(op.substring(2)).date();
                value = formatDate(begin, 0);

                Date end = Moment.moment(begin).endOf(op.substring(2)).date();
                valueEnd = formatDate(end, 0);

                if (isDatetime) {
                    value += ParseHelper.ZERO_TIME;
                    valueEnd += ParseHelper.FULL_TIME;
                }
                op = ParseHelper.BW;

            } else if (ParseHelper.EQ.equalsIgnoreCase(op)
                    && dt == DisplayType.DATETIME && StringUtils.length(value) == 10) {

                op = ParseHelper.BW;
                valueEnd = parseValue(value, op, fieldMeta, true);

            } else if (isREX
                    || ParseHelper.FUD.equalsIgnoreCase(op)
                    || ParseHelper.FUM.equalsIgnoreCase(op)
                    || ParseHelper.FUY.equalsIgnoreCase(op)) {

                int xValue = NumberUtils.toInt(value) * (isREX ? -1 : 1);
                Date date;
                if (ParseHelper.REM.equalsIgnoreCase(op) || ParseHelper.FUM.equalsIgnoreCase(op)) {
                    date = CalendarUtils.addMonth(xValue);
                } else if (ParseHelper.REY.equalsIgnoreCase(op) || ParseHelper.FUY.equalsIgnoreCase(op)) {
                    date = CalendarUtils.addMonth(xValue * 12);
                } else {
                    date = CalendarUtils.addDay(xValue);
                }

                if (isREX) {
                    value = formatDate(date, 0);
                    valueEnd = formatDate(CalendarUtils.now(), 0);
                } else {
                    value = formatDate(CalendarUtils.now(), 0);
                    valueEnd = formatDate(date, 0);
                }

                if (isDatetime) {
                    value += ParseHelper.ZERO_TIME;
                    valueEnd += ParseHelper.FULL_TIME;
                }
                op = ParseHelper.BW;

            }

        } else if (dt == DisplayType.TIME) {
            
            if (value != null && value.length() == 5) {
                if (ParseHelper.EQ.equalsIgnoreCase(op)) {
                    op = ParseHelper.BW;
                    valueEnd = value + ":59";
                }
                value += ":00";
            }

        } else if (dt == DisplayType.MULTISELECT) {
            
            if (ParseHelper.IN.equalsIgnoreCase(op) || ParseHelper.NIN.equalsIgnoreCase(op)) {
                op = ParseHelper.IN.equalsIgnoreCase(op) ? ParseHelper.BAND : ParseHelper.NBAND;

                long maskValue = 0;
                for (String s : value.split("\\|")) {
                    maskValue += ObjectUtils.toLong(s);
                }
                value = maskValue + "";
            }
        }

        StringBuilder sb = new StringBuilder(field)
                .append(' ')
                .append(ParseHelper.convetOperation(op));
        
        if (ParseHelper.NL.equalsIgnoreCase(op) || ParseHelper.NT.equalsIgnoreCase(op)) {
            return sb.toString();
        }

        sb.append(' ');

        

        if (ParseHelper.BFD.equalsIgnoreCase(op)) {
            value = formatDate(addDay(-NumberUtils.toInt(value)), isDatetime ? 1 : 0);
        } else if (ParseHelper.BFM.equalsIgnoreCase(op)) {
            value = formatDate(addMonth(-NumberUtils.toInt(value)), isDatetime ? 1 : 0);
        } else if (ParseHelper.BFY.equalsIgnoreCase(op)) {
            value = formatDate(addMonth(-NumberUtils.toInt(value) * 12), isDatetime ? 1 : 0);
        } else if (ParseHelper.AFD.equalsIgnoreCase(op)) {
            value = formatDate(addDay(NumberUtils.toInt(value)), isDatetime ? 2 : 0);
        } else if (ParseHelper.AFM.equalsIgnoreCase(op)) {
            value = formatDate(addMonth(NumberUtils.toInt(value)), isDatetime ? 2 : 0);
        } else if (ParseHelper.AFY.equalsIgnoreCase(op)) {
            value = formatDate(addMonth(NumberUtils.toInt(value) * 12), isDatetime ? 2 : 0);
        }
        
        else if (ParseHelper.SFU.equalsIgnoreCase(op)) {
            value = getUser().toLiteral();
        } else if (ParseHelper.SFB.equalsIgnoreCase(op)) {
            Department dept = UserHelper.getDepartment(getUser());
            if (dept != null) {
                value = dept.getIdentity().toString();
                int ref = fieldMeta.getReferenceEntity().getEntityCode();
                if (ref == EntityHelper.User) {
                    sb.insert(sb.indexOf(" "), ".deptId");
                } else if (ref == EntityHelper.Department) {
                    
                } else {
                    value = null;
                }
            }
        } else if (ParseHelper.SFD.equalsIgnoreCase(op)) {
            Department dept = UserHelper.getDepartment(getUser());
            if (dept != null) {
                int refe = fieldMeta.getReferenceEntity().getEntityCode();
                if (refe == EntityHelper.Department) {
                    value = StringUtils.join(UserHelper.getAllChildren(dept), "|");
                }
            }
        } else if (ParseHelper.SFT.equalsIgnoreCase(op)) {
            if (value == null) value = "0";  
            
            value = String.format(
                    "( select userId from TeamMember where teamId in ('%s') )",
                    StringUtils.join(value.split("\\|"), "', '"));
        }

        if (StringUtils.isBlank(value)) {
            log.warn("No search value defined : " + item.toJSONString());
            return null;
        }

        
        if (value.matches("\\{\\d+}")) {
            if (values == null || values.isEmpty()) {
                return null;
            }

            String valHold = value.replaceAll("[{}]", "");
            value = parseValue(values.get(valHold), op, fieldMeta, false);
        } else {
            value = parseValue(value, op, fieldMeta, false);
        }

        
        if (value == null) {
            return null;
        }

        
        final boolean isBetween = op.equalsIgnoreCase(ParseHelper.BW);
        if (isBetween && valueEnd == null) {
            valueEnd = parseValue(item.getString("value2"), op, fieldMeta, true);
            if (valueEnd == null) {
                valueEnd = value;
            }
        }

        
        if (op.equalsIgnoreCase(ParseHelper.IN) || op.equalsIgnoreCase(ParseHelper.NIN)
                || op.equalsIgnoreCase(ParseHelper.SFD) || op.equalsIgnoreCase(ParseHelper.SFT)) {
            sb.append(value);
        } else {
            
            if (op.equalsIgnoreCase(ParseHelper.LK) || op.equalsIgnoreCase(ParseHelper.NLK)) {
                value = '%' + value + '%';
            }
            sb.append(quoteValue(value, fieldMeta.getType()));
        }

        if (isBetween) {
            sb.insert(0, "( ")
                    .append(" and ").append(quoteValue(valueEnd, fieldMeta.getType()))
                    .append(" )");
        }

        return sb.toString();
    }

    
    private String parseValue(Object val, String op, Field field, boolean fullTime) {
        String value;
        
        if (val instanceof JSONArray) {
            Set<String> inVals = new HashSet<>();
            for (Object v : (JSONArray) val) {
                inVals.add(quoteValue(v.toString(), field.getType()));
            }
            return optimizeIn(inVals);

        } else {
            value = val.toString();
            if (StringUtils.isBlank(value)) {
                return null;
            }

            
            if (field.getType() == FieldType.TIMESTAMP && StringUtils.length(value) == 10) {
                if (ParseHelper.GT.equalsIgnoreCase(op)) {
                    value += ParseHelper.FULL_TIME;  
                } else if (ParseHelper.LT.equalsIgnoreCase(op)) {
                    value += ParseHelper.ZERO_TIME;  
                } else if (ParseHelper.GE.equalsIgnoreCase(op)) {
                    value += ParseHelper.ZERO_TIME;  
                } else if (ParseHelper.LE.equalsIgnoreCase(op)) {
                    value += ParseHelper.FULL_TIME;  
                } else if (ParseHelper.BW.equalsIgnoreCase(op)) {
                    value += (fullTime ? ParseHelper.FULL_TIME : ParseHelper.ZERO_TIME);  
                }
            }

            
            if (op.equalsIgnoreCase(ParseHelper.IN) || op.equalsIgnoreCase(ParseHelper.NIN)
                    || op.equalsIgnoreCase(ParseHelper.SFD)) {
                Set<String> inVals = new HashSet<>();
                for (String v : value.split("\\|")) {
                    inVals.add(quoteValue(v, field.getType()));
                }
                return optimizeIn(inVals);
            }
        }
        return value;
    }

    
    private String quoteValue(String val, Type type) {
        if (NumberUtils.isNumber(val) && isNumberType(type)) {
            return val;
        } else if (StringUtils.isNotBlank(val)) {
            return String.format("'%s'", StringEscapeUtils.escapeSql(val));
        }
        return "''";
    }

    
    private String optimizeIn(Set<String> inVals) {
        if (inVals == null || inVals.isEmpty()) {
            return null;
        }
        return "( " + StringUtils.join(inVals, ",") + " )";
    }

    
    private boolean isNumberType(Type type) {
        return type == FieldType.INT || type == FieldType.SMALL_INT || type == FieldType.LONG
                || type == FieldType.DOUBLE || type == FieldType.DECIMAL;
    }

    
    private String formatDate(Date date, int paddingType) {
        String s = CalendarUtils.getUTCDateFormat().format(date);
        if (paddingType == 1) s += ParseHelper.FULL_TIME;
        else if (paddingType == 2) s += ParseHelper.ZERO_TIME;
        return s;
    }

    
    private JSONArray buildQuickFilterItems(String quickFields) {
        Set<String> usesFields = ParseHelper.buildQuickFields(rootEntity, quickFields);

        JSONArray items = new JSONArray();
        for (String field : usesFields) {
            items.add(JSON.parseObject("{ op:'LK', value:'{1}', field:'" + field + "' }"));
        }
        return items;
    }

    
    public static String validEquation(String equation) {
        if (StringUtils.isBlank(equation)) {
            return "OR";
        }
        if ("OR".equalsIgnoreCase(equation) || "AND".equalsIgnoreCase(equation)) {
            return equation;
        }

        String clearEquation = equation.toUpperCase()
                .replace("OR", " OR ")
                .replace("AND", " AND ")
                .replaceAll("\\s+", " ")
                .trim();
        equation = clearEquation;

        if (clearEquation.startsWith("AND") || clearEquation.startsWith("OR")
                || clearEquation.endsWith("AND") || clearEquation.endsWith("OR")) {
            return null;
        }
        if (clearEquation.contains("()") || clearEquation.contains("( )")) {
            return null;
        }

        for (String token : clearEquation.split(" ")) {
            token = token.replace("(", "");
            token = token.replace(")", "");

            
            if (NumberUtils.isNumber(token)) {
                if (NumberUtils.toInt(token) > 10) {
                    return null;
                } else {
                    
                }
            } else if ("AND".equals(token) || "OR".equals(token) || "(".equals(token) || ")".equals(token)) {
                
            } else {
                return null;
            }
        }

        
        
        clearEquation = clearEquation.replaceAll("[AND|OR|0-9|\\s]", "");
        
        for (int i = 0; i < 20; i++) {
            clearEquation = clearEquation.replace("()", "");
            if (clearEquation.length() == 0) {
                return equation;
            }
        }
        return null;
    }
}
