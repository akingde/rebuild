/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service;

import cn.devezhao.bizz.privileges.PrivilegesException;
import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.PersistManagerFactory;
import cn.devezhao.persist4j.Record;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.RebuildException;
import com.rebuild.core.metadata.MetadataHelper;
import com.rebuild.core.privileges.PrivilegesGuardInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;


@Service
public class CommonsService extends InternalPersistService {

    protected CommonsService(PersistManagerFactory aPMFactory) {
        super(aPMFactory);
    }

    @Override
    public int getEntityCode() {
        return 0;
    }

    @Override
    public Record create(Record record) {
        return create(record, true);
    }

    @Override
    public Record update(Record record) {
        return update(record, true);
    }

    @Override
    public int delete(ID recordId) {
        return delete(recordId, true);
    }

    
    public Record create(Record record, boolean strictMode) {
        if (strictMode) {
            tryIfHasPrivileges(record);
        }
        return super.create(record);
    }

    
    public Record update(Record record, boolean strictMode) {
        if (strictMode) {
            tryIfHasPrivileges(record);
        }
        return super.update(record);
    }

    
    public int delete(ID recordId, boolean strictMode) {
        if (strictMode) {
            tryIfHasPrivileges(recordId);
        }
        return super.delete(recordId);
    }

    
    public void createOrUpdate(Record[] records) {
        createOrUpdate(records, true);
    }

    
    public void createOrUpdate(Record[] records, boolean strictMode) {
        Assert.notNull(records, "[records] cannot be null");
        for (Record r : records) {
            if (r.getPrimary() == null) create(r, strictMode);
            else update(r, strictMode);
        }
    }

    
    public void delete(ID[] deletes) {
        delete(deletes, true);
    }

    
    public void delete(ID[] deletes, boolean strictMode) {
        Assert.notNull(deletes, "[deletes] cannot be null");
        for (ID id : deletes) {
            delete(id, strictMode);
        }
    }

    
    public void createOrUpdateAndDelete(Record[] records, ID[] deletes, boolean strictMode) {
        createOrUpdate(records, strictMode);
        delete(deletes, strictMode);
    }

    
    private void tryIfHasPrivileges(Object idOrRecord) throws PrivilegesException {
        Entity entity;
        if (idOrRecord instanceof ID) {
            entity = MetadataHelper.getEntity(((ID) idOrRecord).getEntityCode());
        } else if (idOrRecord instanceof Record) {
            entity = ((Record) idOrRecord).getEntity();
        } else {
            throw new RebuildException("Invalid argument [idOrRecord] : " + idOrRecord);
        }

        
        if (entity.getMainEntity() != null) {
            entity = entity.getMainEntity();
        }

        if (MetadataHelper.hasPrivilegesField(entity)) {
            throw new PrivilegesException("Privileges/Business entity cannot use this class (methods) : " + entity.getName());
        }
    }
}
