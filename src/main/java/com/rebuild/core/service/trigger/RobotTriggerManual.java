/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.trigger;

import com.rebuild.core.service.general.OperatingContext;


public class RobotTriggerManual extends RobotTriggerObserver {

    
    public void onApproved(OperatingContext context) {
        execAction(context, TriggerWhen.APPROVED);
    }

    
    public void onRevoked(OperatingContext context) {
        execAction(context, TriggerWhen.REVOKED);
    }

    

    @Override
    public void onUpdate(OperatingContext context) {
        super.onUpdate(context);
    }
}
