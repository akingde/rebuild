/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.trigger;

import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSON;
import com.rebuild.core.metadata.MetadataHelper;


public class ActionContext {

    final private ID sourceRecord;
    final private Entity sourceEntity;
    final private JSON actionContent;
    final private ID configId;

    
    public ActionContext(ID sourceRecord, Entity sourceEntity, JSON actionContent, ID configId) {
        this.sourceRecord = sourceRecord;
        this.sourceEntity = sourceRecord != null ? MetadataHelper.getEntity(sourceRecord.getEntityCode()) : sourceEntity;
        this.actionContent = actionContent;
        this.configId = configId;
    }

    
    public Entity getSourceEntity() {
        return sourceEntity;
    }

    
    public ID getSourceRecord() {
        return sourceRecord;
    }

    
    public JSON getActionContent() {
        return actionContent;
    }

    
    public ID getConfigId() {
        return configId;
    }
}
