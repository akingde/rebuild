/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.trigger;

import com.rebuild.core.service.general.OperatingContext;


public abstract class TriggerAction {

    final protected ActionContext actionContext;

    protected TriggerAction(ActionContext actionContext) {
        this.actionContext = actionContext;
    }

    public ActionContext getActionContext() {
        return actionContext;
    }

    abstract public ActionType getType();

    abstract public void execute(OperatingContext operatingContext) throws TriggerException;

    
    protected void prepare(OperatingContext operatingContext) throws TriggerException {
    }

    
    public void clean() {
    }

    
    public boolean isUsableSourceEntity(int entityCode) {
        return true;
    }

    @Override
    public String toString() {
        return super.toString() + "#" + actionContext.getConfigId();
    }
}
