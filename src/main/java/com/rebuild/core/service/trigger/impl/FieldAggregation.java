/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.trigger.impl;

import cn.devezhao.commons.CalendarUtils;
import cn.devezhao.commons.ObjectUtils;
import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.Record;
import cn.devezhao.persist4j.engine.ID;
import cn.devezhao.persist4j.metadata.MissingMetaExcetion;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.Application;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.metadata.MetadataHelper;
import com.rebuild.core.metadata.easymeta.DisplayType;
import com.rebuild.core.metadata.easymeta.EasyMetaFactory;
import com.rebuild.core.privileges.PrivilegesGuardContextHolder;
import com.rebuild.core.privileges.UserService;
import com.rebuild.core.service.ServiceSpec;
import com.rebuild.core.service.general.GeneralEntityServiceContextHolder;
import com.rebuild.core.service.general.OperatingContext;
import com.rebuild.core.service.query.AdvFilterParser;
import com.rebuild.core.service.trigger.ActionContext;
import com.rebuild.core.service.trigger.ActionType;
import com.rebuild.core.service.trigger.TriggerAction;
import com.rebuild.core.service.trigger.TriggerException;
import com.rebuild.utils.CommonsUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Slf4j
public class FieldAggregation extends TriggerAction {

    
    public static final String SOURCE_SELF = "$PRIMARY$";

    
    final protected int maxTriggerDepth;
    
    
    protected static final ThreadLocal<List<String>> TRIGGER_CHAIN = new ThreadLocal<>();

    
    protected Entity sourceEntity;
    
    protected Entity targetEntity;

    
    protected ID targetRecordId;
    
    protected String followSourceWhere;

    public FieldAggregation(ActionContext context) {
        this(context, 9);
    }

    protected FieldAggregation(ActionContext context, int maxTriggerDepth) {
        super(context);
        this.maxTriggerDepth = maxTriggerDepth;
    }

    @Override
    public ActionType getType() {
        return ActionType.FIELDAGGREGATION;
    }

    
    protected List<String> checkTriggerChain(String chainName) {
        List<String> tschain = TRIGGER_CHAIN.get();
        if (tschain == null) {
            tschain = new ArrayList<>();
        } else {
            log.info("Occured trigger-chain : {} > {} (current)", StringUtils.join(tschain, " > "), chainName);

            
            if (tschain.contains(chainName)) {
                if (Application.devMode()) log.warn("[dev] Record triggered only once on trigger-chain : {}", chainName);
                return null;
            }
        }

        if (tschain.size() >= maxTriggerDepth) {
            throw new TriggerException("Exceed the maximum trigger depth : " + StringUtils.join(tschain, " > "));
        }

        return tschain;
    }

    @Override
    public void execute(OperatingContext operatingContext) throws TriggerException {
        final String chainName = actionContext.getConfigId() + ":" + operatingContext.getAction().getName();
        final List<String> tschain = checkTriggerChain(chainName);
        if (tschain == null) return;

        this.prepare(operatingContext);
        if (targetRecordId == null) {
            log.warn("No target record found");
            return;
        }

        
        JSONObject dataFilter = ((JSONObject) actionContext.getActionContent()).getJSONObject("dataFilter");
        String dataFilterSql = null;
        if (dataFilter != null && !dataFilter.isEmpty()) {
            dataFilterSql = new AdvFilterParser(dataFilter).toSqlWhere();
        }

        
        Record targetRecord = EntityHelper.forUpdate(targetRecordId, UserService.SYSTEM_USER, false);

        JSONArray items = ((JSONObject) actionContext.getActionContent()).getJSONArray("items");
        for (Object o : items) {
            JSONObject item = (JSONObject) o;
            String targetField = item.getString("targetField");
            if (!MetadataHelper.checkAndWarnField(targetEntity, targetField)) {
                continue;
            }

            String filterSql = followSourceWhere;
            if (dataFilterSql != null) {
                filterSql = String.format("( %s ) and ( %s )", followSourceWhere, dataFilterSql);
            }

            Object evalValue = new AggregationEvaluator(item, sourceEntity, filterSql).eval();
            if (evalValue == null) continue;

            DisplayType dt = EasyMetaFactory.getDisplayType(targetEntity.getField(targetField));
            if (dt == DisplayType.NUMBER) {
                targetRecord.setLong(targetField, CommonsUtils.toLongHalfUp(evalValue));
            } else if (dt == DisplayType.DECIMAL) {
                targetRecord.setDouble(targetField, ObjectUtils.toDouble(evalValue));
            } else if (dt == DisplayType.DATE || dt == DisplayType.DATETIME) {
                if (evalValue instanceof Date) targetRecord.setDate(targetField, (Date) evalValue);
                else targetRecord.setNull(targetField);
            } else {
                log.warn("Unsupported file-type {} with {}", dt, targetRecordId);
            }
        }

        
        if (!targetRecord.isEmpty()) {
            final boolean forceUpdate = ((JSONObject) actionContext.getActionContent()).getBooleanValue("forceUpdate");

            
            PrivilegesGuardContextHolder.setSkipGuard(targetRecordId);
            
            if (forceUpdate) {
                GeneralEntityServiceContextHolder.setAllowForceUpdate(targetRecordId);
            }

            
            tschain.add(chainName);
            TRIGGER_CHAIN.set(tschain);

            ServiceSpec useService = MetadataHelper.isBusinessEntity(targetEntity)
                    ? Application.getEntityService(targetEntity.getEntityCode())
                    : Application.getService(targetEntity.getEntityCode());

            targetRecord.setDate(EntityHelper.ModifiedOn, CalendarUtils.now());
            try {
                useService.update(targetRecord);
            } finally {
                PrivilegesGuardContextHolder.getSkipGuardOnce();
                GeneralEntityServiceContextHolder.isAllowForceUpdateOnce();
            }
        }
    }

    @Override
    public void prepare(OperatingContext operatingContext) throws TriggerException {
        if (sourceEntity != null) return;  

        
        String[] targetFieldEntity = ((JSONObject) actionContext.getActionContent()).getString("targetEntity").split("\\.");
        sourceEntity = actionContext.getSourceEntity();
        targetEntity = MetadataHelper.getEntity(targetFieldEntity[1]);

        String followSourceField;

        
        if (SOURCE_SELF.equalsIgnoreCase(targetFieldEntity[0])) {
            followSourceField = sourceEntity.getPrimaryField().getName();
            targetRecordId = actionContext.getSourceRecord();
        } else {
            followSourceField = targetFieldEntity[0];
            if (!sourceEntity.containsField(followSourceField)) {
                throw new MissingMetaExcetion(followSourceField, sourceEntity.getName());
            }

            
            Object[] o = Application.getQueryFactory().uniqueNoFilter(
                    actionContext.getSourceRecord(), followSourceField, followSourceField + "." + EntityHelper.CreatedBy);
            
            if (o != null && o[0] != null && o[1] != null) {
                targetRecordId = (ID) o[0];
            }
        }

        this.followSourceWhere = String.format("%s = '%s'", followSourceField, targetRecordId);
    }

    @Override
    public void clean() {
        TRIGGER_CHAIN.remove();
    }
}
