/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service;

import com.rebuild.core.Application;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.interceptor.TransactionAspectSupport;


public class TransactionManual {

    
    public static TransactionStatus newTransaction() {
        DefaultTransactionAttribute attr = new DefaultTransactionAttribute();
        attr.setName("rb-txm-" + RandomStringUtils.randomNumeric(12));
        return getTxManager().getTransaction(attr);
    }

    
    public static TransactionStatus currentTransaction() {
        return TransactionAspectSupport.currentTransactionStatus();
    }

    
    public static void commit(TransactionStatus status) {
        getTxManager().commit(status);
        status.flush();
    }

    
    public static void rollback(TransactionStatus status) {
        getTxManager().rollback(status);
        status.flush();
    }

    
    protected static DataSourceTransactionManager getTxManager() {
        return Application.getBean(DataSourceTransactionManager.class);
    }

}
