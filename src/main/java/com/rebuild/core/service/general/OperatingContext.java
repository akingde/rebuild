/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.general;

import cn.devezhao.bizz.privileges.Permission;
import cn.devezhao.persist4j.Record;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.UserContextHolder;
import org.springframework.util.Assert;


public class OperatingContext {

    final private ID operator;
    final private Permission action;

    final private Record beforeRecord;
    final private Record afterRecord;

    final private ID[] affected;

    final private String operationIp;

    
    private OperatingContext(ID operator, Permission action, Record beforeRecord, Record afterRecord, ID[] affected, String operationIp) {
        Assert.isTrue(beforeRecord != null || afterRecord != null,
                "At least one of `beforeRecord` or `afterRecord` is not null");

        this.operator = operator;
        this.action = action;
        this.beforeRecord = beforeRecord;
        this.afterRecord = afterRecord;
        this.affected = affected == null ? new ID[]{getAnyRecord().getPrimary()} : affected;
        this.operationIp = operationIp;
    }

    
    public ID getOperator() {
        return operator;
    }

    
    public Permission getAction() {
        return action;
    }

    
    public Record getBeforeRecord() {
        return beforeRecord;
    }

    
    public Record getAfterRecord() {
        return afterRecord;
    }

    
    public Record getAnyRecord() {
        return getAfterRecord() != null ? getAfterRecord() : getBeforeRecord();
    }

    
    public ID[] getAffected() {
        return affected;
    }

    
    public String getOperationIp() {
        return operationIp;
    }

    @Override
    public String toString() {
        return String.format("[ Action:%s, Record(s):%s(%d) ]",
                getAction().getName(), getAnyRecord().getPrimary(), getAffected().length);
    }

    
    public static OperatingContext create(ID operator, Permission action, Record before, Record after) {
        return create(operator, action, before, after, null);
    }

    
    public static OperatingContext create(ID operator, Permission action, Record before, Record after, ID[] affected) {
        return new OperatingContext(operator, action, before, after, affected, UserContextHolder.getReqip());
    }
}
