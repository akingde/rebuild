/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.general.series;

import cn.devezhao.commons.CalendarUtils;


public class TimeVar extends SeriesVar {

    
    protected TimeVar(String symbols) {
        super(symbols);
    }

    @Override
    public String generate() {
        String s = getSymbols().replace("Y", "y");
        
        s = s.replace("D", "d");
        s = s.replace("I", "m");
        s = s.replace("S", "s");
        return CalendarUtils.format(s, CalendarUtils.now());
    }
}
