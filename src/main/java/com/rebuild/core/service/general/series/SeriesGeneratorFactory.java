/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.general.series;

import cn.devezhao.persist4j.Field;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.metadata.easymeta.EasyMetaFactory;


public class SeriesGeneratorFactory {

    
    public static SeriesGenerator create(Field field) {
        return new SeriesGenerator(EasyMetaFactory.valueOf(field));
    }

    
    public static String generate(Field field) {
        return create(field).generate();
    }

    
    public static String preview(JSONObject config) {
        return new SeriesGenerator(null, config).generate();
    }

    
    protected static void zero(Field field) {
        new IncreasingVar(field).clean();
    }
}
