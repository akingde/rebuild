/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.dataimport;

import cn.devezhao.commons.excel.CSVReader;
import cn.devezhao.commons.excel.Cell;
import com.rebuild.utils.ExcelUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class DataFileParser {

    final private File sourceFile;
    final private String encoding;

    
    public DataFileParser(File sourceFile) {
        this(sourceFile, "utf-8");
    }

    
    public DataFileParser(File sourceFile, String encoding) {
        this.sourceFile = sourceFile;
        this.encoding = encoding;
    }

    
    public File getSourceFile() {
        return sourceFile;
    }

    
    public int getRowsCount() {
        return parse().size();
    }

    
    public List<Cell[]> parse() {
        return parse(Integer.MAX_VALUE);
    }

    
    public List<Cell[]> parse(int maxRows) {
        if (sourceFile.getName().endsWith(".csv")) {
            return parseCsv(maxRows);
        } else {
            return ExcelUtils.readExcel(this.sourceFile, maxRows, true);
        }
    }

    
    private List<Cell[]> parseCsv(int maxRows) {
        final List<Cell[]> rows = new ArrayList<>();
        try (CSVReader csvReader = new CSVReader(this.sourceFile, this.encoding)) {
            while (csvReader.hasNext()) {
                rows.add(csvReader.next());
                if (rows.size() >= maxRows) {
                    break;
                }
            }
        }
        return rows;
    }
}
