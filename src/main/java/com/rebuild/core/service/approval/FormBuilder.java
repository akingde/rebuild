/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.approval;

import cn.devezhao.persist4j.Entity;
import cn.devezhao.persist4j.Record;
import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSONArray;
import com.rebuild.core.configuration.general.FormsBuilder;


public class FormBuilder {

    final private ID record;
    final private ID user;

    
    public FormBuilder(ID record, ID user) {
        this.record = record;
        this.user = user;
    }

    
    public JSONArray build(JSONArray elements) {
        Record data = UseFormsBuilder.instance.findRecord(record, user, elements);
        UseFormsBuilder.instance.buildModelElements(elements, data.getEntity(), data, user);
        return elements;
    }

    
    static class UseFormsBuilder extends FormsBuilder {
        public static final UseFormsBuilder instance = new UseFormsBuilder();

        protected void buildModelElements(JSONArray elements, Entity entity, Record data, ID user) {
            super.buildModelElements(elements, entity, data, user, false);
        }

        protected Record findRecord(ID id, ID user, JSONArray elements) {
            return super.findRecord(id, user, elements);
        }
    }
}
