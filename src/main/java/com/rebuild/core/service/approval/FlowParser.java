/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.approval;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.support.i18n.Language;
import com.rebuild.utils.JSONUtils;
import org.apache.commons.lang.StringUtils;

import java.util.*;


public class FlowParser {

    private static final JSONObject EMPTY_FLOWS = JSONUtils.toJSONObject("nodes", new Object[0]);

    final private JSON flowDefinition;

    private final Map<String, FlowNode> nodeMap = new HashMap<>();

    
    public FlowParser(JSON flowDefinition) {
        this.flowDefinition = flowDefinition == null ? EMPTY_FLOWS : flowDefinition;
        preparedNodes(((JSONObject) this.flowDefinition).getJSONArray("nodes"), null);
    }

    
    private void preparedNodes(JSONArray nodes, FlowBranch ownBranch) {
        String prevNode = null;
        if (ownBranch != null) {
            prevNode = ownBranch.getNodeId();
        }

        for (Object o : nodes) {
            
            JSONObject node = (JSONObject) o;
            String nodeId = node.getString("nodeId");
            if (!FlowNode.TYPE_CONDITION.equals(node.getString("type"))) {
                FlowNode flowNode = FlowNode.valueOf(node);
                if (prevNode != null) {
                    flowNode.prevNodes = prevNode;
                }
                prevNode = nodeId;
                nodeMap.put(nodeId, flowNode);

                if (ownBranch != null) {
                    ownBranch.addNode(nodeId);
                }
            }

            
            JSONArray branches = node.getJSONArray("branches");
            if (branches != null) {
                Set<String> prevNodes = new HashSet<>();
                for (Object b : branches) {
                    JSONObject branch = (JSONObject) b;
                    String branchNodeId = branch.getString("nodeId");
                    FlowBranch flowBranch = FlowBranch.valueOf(branch);
                    if (prevNode != null) {
                        flowBranch.prevNodes = prevNode;
                    }
                    nodeMap.put(branchNodeId, flowBranch);

                    preparedNodes(branch.getJSONArray("nodes"), flowBranch);
                    prevNodes.add(flowBranch.getLastNode());
                }
                prevNode = StringUtils.join(prevNodes, "|");
            }
        }
    }

    
    public List<FlowNode> getNextNodes(String nodeId) {
        List<FlowNode> next = new ArrayList<>();
        for (FlowNode node : getAllNodes()) {
            if (node.prevNodes != null && node.prevNodes.contains(nodeId)) {
                next.add(node);
            }
        }

        if (next.isEmpty()) {
            return Collections.emptyList();
        }

        
        if (!FlowNode.TYPE_BRANCH.equals(next.get(0).getType())) {
            return next;
        }

        
        next.sort((o1, o2) -> {
            int p1 = ((FlowBranch) o1).getPriority();
            int p2 = ((FlowBranch) o2).getPriority();
            return Integer.compare(p1, p2);
        });
        return next;
    }

    
    public FlowNode getNode(String nodeId) {
        if (nodeMap.containsKey(nodeId)) {
            return nodeMap.get(nodeId);
        }
        throw new ApprovalException(Language.L("无效审批步骤节点 (%s)", nodeId));
    }

    
    public boolean hasApproverNode() {
        for (FlowNode node : nodeMap.values()) {
            if (node.getType().equals(FlowNode.TYPE_APPROVER)) return true;
        }
        return false;
    }

    
    protected Collection<FlowNode> getAllNodes() {
        return nodeMap.values();
    }

    
    protected JSON getFlowDefinition() {
        return flowDefinition;
    }

    
    protected void prettyPrint(String nodeId, String space) {
        space = space == null ? "" : space;
        FlowNode node = getNode(nodeId);
        System.out.println(space + node);

        List<FlowNode> next = this.getNextNodes(nodeId);
        for (FlowNode n : next) {
            prettyPrint(n.getNodeId(), space + "  ");
        }
    }
}
