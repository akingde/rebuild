/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.approval;

import cn.devezhao.persist4j.Field;
import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.Application;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.privileges.UserHelper;
import com.rebuild.core.privileges.bizz.Department;
import com.rebuild.utils.JSONUtils;
import org.apache.commons.lang.StringUtils;

import java.util.*;


@SuppressWarnings("unused")
public class FlowNode {

    

    public static final String NODE_ROOT = "ROOT";
    public static final String NODE_CANCELED = "CANCELED";
    public static final String NODE_REVOKED = "REVOKED";
    public static final String NODE_AUTOAPPROVAL = "AUTOAPPROVAL";

    

    public static final String TYPE_START = "start";
    public static final String TYPE_APPROVER = "approver";
    public static final String TYPE_CC = "cc";
    public static final String TYPE_CONDITION = "condition";
    public static final String TYPE_BRANCH = "branch";

    

    public static final String USER_ALL = "ALL";
    public static final String USER_SELF = "SELF";
    public static final String USER_SPEC = "SPEC";
    public static final String USER_OWNS = "OWNS";

    

    public static final String SIGN_AND = "AND";  
    public static final String SIGN_OR = "OR";      
    public static final String SIGN_ALL = "ALL";  

    

    private String nodeId;
    private String type;
    private JSONObject dataMap;

    protected String prevNodes;

    
    protected FlowNode(String nodeId, String type, JSONObject dataMap) {
        super();
        this.nodeId = nodeId;
        this.type = type;
        this.dataMap = dataMap;
    }

    
    public String getNodeId() {
        return nodeId;
    }

    
    public String getType() {
        return type;
    }

    
    public JSONObject getDataMap() {
        return dataMap == null ? JSONUtils.EMPTY_OBJECT : dataMap;
    }

    
    public String getSignMode() {
        return StringUtils.defaultIfBlank(getDataMap().getString("signMode"), SIGN_OR);
    }

    
    public boolean allowSelfSelecting() {
        if (getDataMap().containsKey("selfSelecting")) {
            return getDataMap().getBooleanValue("selfSelecting");
        } else {
            
            return true;
        }
    }

    
    public boolean allowCcAutoShare() {
        if (getDataMap().containsKey("ccAutoShare")) {
            return getDataMap().getBooleanValue("ccAutoShare");
        } else {
            
            return true;
        }
    }

    
    public Set<ID> getSpecUsers(ID operator, ID record) {
        JSONArray userDefs = getDataMap().getJSONArray("users");
        if (userDefs == null || userDefs.isEmpty()) {
            return Collections.emptySet();
        }

        String userType = userDefs.getString(0);
        if (USER_SELF.equalsIgnoreCase(userType)) {
            Set<ID> users = new HashSet<>();
            ID owning = Application.getRecordOwningCache().getOwningUser(record);
            users.add(owning);
            return users;
        }

        Set<ID> users = new HashSet<>();

        List<String> defsList = new ArrayList<>();
        for (Object o : userDefs) {
            String def = (String) o;
            if (def.startsWith(ApprovalHelper.APPROVAL_SUBMITOR) || def.startsWith(ApprovalHelper.APPROVAL_APPROVER)) {
                ApprovalState state = ApprovalHelper.getApprovalState(record);
                boolean isSubmitted = state == ApprovalState.PROCESSING || state == ApprovalState.APPROVED;

                ID whichUser = operator;

                if (def.startsWith(ApprovalHelper.APPROVAL_SUBMITOR)) {
                    if (isSubmitted) {
                        whichUser = ApprovalHelper.getSubmitter(record);
                    } else {
                        
                    }
                } else {
                    if (isSubmitted) {
                        
                    } else {
                        whichUser = null;  
                    }
                }

                if (whichUser != null) {
                    Field userField = ApprovalHelper.checkVirtualField(def);
                    if (userField != null) {
                        Object[] ud;
                        
                        if (userField.getOwnEntity().getEntityCode() == EntityHelper.Department) {
                            Department d = Application.getUserStore().getUser(whichUser).getOwningDept();
                            ud = Application.getQueryFactory().uniqueNoFilter((ID) d.getIdentity(), userField.getName());
                        } else {
                            ud = Application.getQueryFactory().uniqueNoFilter(whichUser, userField.getName());
                        }

                        if (ud != null && ud[0] != null) {
                            if (userField.getReferenceEntity().getEntityCode() == EntityHelper.Department) {
                                defsList.add(ud[0].toString());
                            } else {
                                users.add((ID) ud[0]);
                            }
                        }
                    }
                }

            } else {
                defsList.add(def);
            }
        }

        users.addAll(UserHelper.parseUsers(defsList, record));
        users.removeIf(id -> !UserHelper.isActive(id));

        return users;
    }

    @Override
    public String toString() {
        String string = String.format("Id:%s, Type:%s", nodeId, type);
        if (prevNodes != null) {
            string += ", Prev:" + prevNodes;
        }
        if (dataMap != null) {
            string += ", Data:" + dataMap.toJSONString();
        }
        return string;
    }

    @Override
    public int hashCode() {
        return this.nodeId.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        return obj instanceof FlowNode && obj.hashCode() == this.hashCode();
    }

    
    public JSONArray getEditableFields() {
        JSONArray editableFields = dataMap == null ? null : dataMap.getJSONArray("editableFields");
        if (editableFields == null) {
            return null;
        }

        editableFields = (JSONArray) JSONUtils.clone(editableFields);
        for (Object o : editableFields) {
            JSONObject field = (JSONObject) o;
            field.put("nullable", !((Boolean) field.remove("notNull")));
        }
        return editableFields;
    }

    

    
    public static FlowNode valueOf(JSONObject node) {
        return new FlowNode(
                node.getString("nodeId"), node.getString("type"), node.getJSONObject("data"));
    }
}
