/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.approval;

import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSONObject;
import com.rebuild.core.privileges.UserHelper;
import org.springframework.util.Assert;

import java.util.HashSet;
import java.util.Set;


public class FlowNodeGroup {

    private Set<FlowNode> nodes = new HashSet<>();

    protected FlowNodeGroup() {
        super();
    }

    
    public void addNode(FlowNode node) {
        Assert.isNull(getApprovalNode(), "Cannot add multiple approved nodes");
        nodes.add(node);
    }

    
    public boolean allowSelfSelectingCc() {
        for (FlowNode node : nodes) {
            if (node.getType().equals(FlowNode.TYPE_CC) && node.allowSelfSelecting()) {
                return true;
            }
        }
        return false;
    }

    
    public boolean allowSelfSelectingApprover() {
        FlowNode node = getApprovalNode();
        return node != null && node.allowSelfSelecting();
    }

    
    public Set<ID> getCcUsers(ID operator, ID recordId, JSONObject selectUsers) {
        Set<ID> users = new HashSet<>();
        
        for (FlowNode node : nodes) {
            if (FlowNode.TYPE_CC.equals(node.getType())) {
                users.addAll(node.getSpecUsers(operator, recordId));
            }
        }

        if (selectUsers != null) {
            users.addAll(UserHelper.parseUsers(selectUsers.getJSONArray("selectCcs"), recordId));
        }
        return users;
    }

    
    public Set<ID> getCcUsers4Share(ID operator, ID recordId, JSONObject selectUsers) {
        Set<ID> users = new HashSet<>();
        FlowNode firstNode = null;
        for (FlowNode node : nodes) {
            if (FlowNode.TYPE_CC.equals(node.getType()) && node.allowCcAutoShare()) {
                users.addAll(node.getSpecUsers(operator, recordId));

                if (firstNode == null) {
                    firstNode = node;
                }
            }
        }

        
        if (firstNode != null && selectUsers != null) {
            users.addAll(UserHelper.parseUsers(selectUsers.getJSONArray("selectCcs"), recordId));
        }
        return users;
    }

    
    public Set<ID> getApproveUsers(ID operator, ID recordId, JSONObject selectUsers) {
        Set<ID> users = new HashSet<>();

        FlowNode node = getApprovalNode();
        if (node != null) {
            users.addAll(node.getSpecUsers(operator, recordId));
        }

        if (selectUsers != null) {
            users.addAll(UserHelper.parseUsers(selectUsers.getJSONArray("selectApprovers"), recordId));
        }
        return users;
    }

    
    public boolean isLastStep() {
        
        return getApprovalNode() == null;
    }

    
    public boolean isValid() {
        return !nodes.isEmpty();
    }

    
    public FlowNode getApprovalNode() {
        for (FlowNode node : nodes) {
            if (FlowNode.TYPE_APPROVER.equals(node.getType())) {
                return node;
            }
        }
        return null;
    }

    
    public String getSignMode() {
        FlowNode node = getApprovalNode();
        return node == null ? FlowNode.SIGN_OR : node.getSignMode();
    }

    
    public String getGroupId() {
        StringBuilder sb = new StringBuilder();
        for (FlowNode node : nodes) {
            sb.append(node.getNodeId());
        }
        return sb.toString();
    }
}
