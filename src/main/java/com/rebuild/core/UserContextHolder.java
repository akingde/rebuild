/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core;

import cn.devezhao.bizz.security.AccessDeniedException;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.support.ConfigurationItem;
import com.rebuild.core.support.RebuildConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.NamedThreadLocal;
import org.springframework.util.Assert;


@Slf4j
public class UserContextHolder {

    private static final ThreadLocal<ID> CALLER = new NamedThreadLocal<>("Current user");

    private static final ThreadLocal<String> LOCALE = new NamedThreadLocal<>("Request locale");

    private static final ThreadLocal<String> REQIP = new NamedThreadLocal<>("Request IP");

    private UserContextHolder() { }

    
    public static void set(ID user, String locale) {
        setUser(user);
        setLocale(locale);
    }

    
    public static void setUser(ID user) {
        Assert.notNull(user, "[user] cannot be null");

        ID exists = getUser(true);
        if (exists != null) {
            log.warn("Replace user in current session (thread) : " + exists + " > " + user);
            CALLER.remove();
        }
        CALLER.set(user);
    }

    
    public static void setLocale(String locale) {
        Assert.notNull(locale, "[locale] cannot be null");
        LOCALE.set(locale);
    }

    
    public static ID getUser() {
        return getUser(false);
    }

    
    public static ID getUser(boolean allowNull) throws AccessDeniedException {
        ID user = CALLER.get();
        if (user != null) return user;

        if (allowNull) return null;
        else throw new AccessDeniedException("No user found in current session (thread)");
    }

    
    public static String getLocale() {
        String local = LOCALE.get();
        if (local != null) return local;

        
        return RebuildConfiguration.get(ConfigurationItem.DefaultLanguage);
    }

    
    public static void clear() {
        clearUser();
        clearLocale();
        REQIP.remove();
    }

    
    public static void clearUser() {
        CALLER.remove();
    }

    
    public static void clearLocale() {
        LOCALE.remove();
    }

    

    
    public static void setReqip(String reqip) {
        Assert.notNull(reqip, "[reqip] cannot be null");
        REQIP.set(reqip);
    }

    
    public static String getReqip() {
        return REQIP.get();
    }
}
