/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.web;

import cn.devezhao.commons.web.ServletUtils;
import cn.devezhao.persist4j.engine.ID;
import com.alibaba.fastjson.JSON;
import com.rebuild.api.Controller;
import com.rebuild.core.support.i18n.Language;
import com.rebuild.utils.AppUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;


public abstract class BaseController extends Controller {

    
    protected ID getRequestUser(HttpServletRequest request) {
        ID user = AppUtils.getRequestUser(request);
        if (user == null) {
            throw new InvalidParameterException(Language.L("无效请求用户"));
        }
        return user;
    }

    
    protected void writeSuccess(HttpServletResponse response) {
        writeSuccess(response, null);
    }

    
    protected void writeSuccess(HttpServletResponse response, Object data) {
        writeJSON(response, formatSuccess(data));
    }

    
    protected void writeFailure(HttpServletResponse response) {
        writeFailure(response, null);
    }

    
    protected void writeFailure(HttpServletResponse response, String message) {
        writeJSON(response, formatFailure(message));
    }

    
    protected void writeFailure(HttpServletResponse response, String message, int errorCode) {
        writeJSON(response, formatFailure(message, errorCode));
    }

    private void writeJSON(HttpServletResponse response, Object aJson) {
        String aJsonString;
        if (aJson instanceof String) {
            aJsonString = (String) aJson;
        } else {
            aJsonString = JSON.toJSONString(aJson);
        }
        ServletUtils.writeJson(response, aJsonString);
    }

    
    protected String getParameter(HttpServletRequest request, String name) {
        return request.getParameter(name);
    }

    
    protected String getParameter(HttpServletRequest request, String name, String defaultValue) {
        return StringUtils.defaultIfBlank(getParameter(request, name), defaultValue);
    }

    
    protected String getParameterNotNull(HttpServletRequest request, String name) {
        String v = request.getParameter(name);
        if (StringUtils.isEmpty(v)) {
            throw new InvalidParameterException(Language.L("无效请求参数 (%s=%s)", name, v));
        }
        return v;
    }

    
    protected Integer getIntParameter(HttpServletRequest request, String name) {
        return getIntParameter(request, name, null);
    }

    
    protected Integer getIntParameter(HttpServletRequest request, String name, Integer defaultValue) {
        String v = request.getParameter(name);
        if (StringUtils.isBlank(v)) return defaultValue;

        try {
            return Integer.parseInt(v);
        } catch (NumberFormatException ignored) {
            return defaultValue;
        }
    }

    
    protected boolean getBoolParameter(HttpServletRequest request, String name) {
        String v = request.getParameter(name);
        return v != null && BooleanUtils.toBoolean(v);
    }

    
    protected boolean getBoolParameter(HttpServletRequest request, String name, boolean defaultValue) {
        String v = request.getParameter(name);
        return v == null ? defaultValue : BooleanUtils.toBoolean(v);
    }

    
    protected ID getIdParameter(HttpServletRequest request, String name) {
        String v = request.getParameter(name);
        return ID.isId(v) ? ID.valueOf(v) : null;
    }

    
    protected ID getIdParameterNotNull(HttpServletRequest request, String name) {
        String v = request.getParameter(name);
        if (ID.isId(v)) return ID.valueOf(v);
        throw new InvalidParameterException(Language.L("无效请求参数 (%s=%s)", name, v));
    }

    
    protected ModelAndView createModelAndView(String view) {
        return new ModelAndView(view);
    }

    
    protected ModelAndView createModelAndView(String view, Map<String, Object> modelMap) {
        ModelAndView mv = createModelAndView(view);
        if (modelMap != null && !modelMap.isEmpty()) {
            mv.getModelMap().putAll(modelMap);
        }
        return mv;
    }
}
