/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.utils;

import cn.devezhao.commons.web.ServletUtils;
import cn.devezhao.commons.web.WebUtils;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.api.user.AuthTokenManager;
import com.rebuild.core.Application;
import com.rebuild.core.BootApplication;
import com.rebuild.core.support.ConfigurationItem;
import com.rebuild.core.support.RebuildConfiguration;
import com.rebuild.core.support.i18n.LanguageBundle;
import com.rebuild.web.admin.AdminVerfiyController;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;


public class AppUtils {

    
    public static final String HF_AUTHTOKEN = "X-AuthToken";
    public static final String URL_AUTHTOKEN = "_authToken";

    
    public static final String SK_LOCALE = WebUtils.KEY_PREFIX + ".LOCALE";
    public static final String CK_LOCALE = "rb.locale";

    
    public static final String HF_CLIENT = "X-Client";
    public static final String HF_LOCALE = "X-ClientLocale";

    
    public static String getContextPath() {
        return BootApplication.getContextPath();
    }

    
    public static String getContextPath(String path) {
        if (!path.startsWith("/")) path = "/" + path;
        return BootApplication.getContextPath() + path;
    }

    
    public static ID getRequestUser(HttpServletRequest request) {
        return getRequestUser(request, false);
    }

    
    public static ID getRequestUser(HttpServletRequest request, boolean refreshToken) {
        Object user = request.getSession().getAttribute(WebUtils.CURRENT_USER);
        if (user == null) {
            user = getRequestUserViaToken(request, refreshToken);
        }
        return user == null ? null : (ID) user;
    }

    
    public static ID getRequestUserViaToken(HttpServletRequest request, boolean refreshToken) {
        String authToken = request.getHeader(HF_AUTHTOKEN);
        ID user = AuthTokenManager.verifyToken(authToken, false);
        if (user != null && refreshToken) {
            AuthTokenManager.refreshToken(authToken, AuthTokenManager.H5TOKEN_EXPIRES);
        }
        return user;
    }

    
    public static LanguageBundle getReuqestBundle(HttpServletRequest request) {
        return Application.getLanguage().getBundle(getReuqestLocale(request));
    }

    
    public static String getReuqestLocale(HttpServletRequest request) {
        
        String locale = request.getParameter("locale");
        
        if (locale == null) locale = (String) ServletUtils.getSessionAttribute(request, SK_LOCALE);
        
        if (locale == null) locale = request.getHeader(HF_LOCALE);
        
        if (StringUtils.isBlank(locale)) locale = RebuildConfiguration.get(ConfigurationItem.DefaultLanguage);
        return locale;
    }

    
    public static boolean isAdminVerified(HttpServletRequest request) {
        return ServletUtils.getSessionAttribute(request, AdminVerfiyController.KEY_VERIFIED) != null;
    }

    
    public static boolean isRbMobile(HttpServletRequest request) {
        String UA = request.getHeader(HF_CLIENT);
        return UA != null && UA.startsWith("RB/Mobile-");
    }

    
    public static boolean isIE11(HttpServletRequest request) {
        
        String ua = request.getHeader("user-agent");
        return ua != null && ua.contains("Trident/") && ua.contains("rv:11.");
    }

    
    public static boolean isMobile(HttpServletRequest request) {
        String ua = request.getHeader("user-agent");
        return ua != null && (ua.contains("Mobile") || ua.contains("iPhone") || ua.contains("Android"));
    }
}
