/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.api.sdk;

import cn.devezhao.commons.EncryptUtils;
import com.alibaba.fastjson.JSON;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;


public class OpenApiSDK {

    private static final MediaType JSON_TYPE = MediaType.get("application/json; charset=utf-8");

    private static final JSON ERROR_REQ = JSON.parseObject("{ error_code:600, error_msg:'Http request failed' }");

    private static final Logger LOG = LoggerFactory.getLogger(OpenApiSDK.class);

    final private String appId;
    final private String appSecret;
    final private String baseUrl;

    final private OkHttpClient okHttpClient;

    
    public OpenApiSDK(String appId, String appSecret) {
        this(appId, appSecret, "https://nightly.getrebuild.com/gw/api/");
    }

    
    public OpenApiSDK(String appId, String appSecret, String baseUrl) {
        this.appId = appId;
        this.appSecret = appSecret;
        this.baseUrl = baseUrl;

        this.okHttpClient = new OkHttpClient().newBuilder()
                .retryOnConnectionFailure(false)
                .callTimeout(15, TimeUnit.SECONDS)
                .build();
    }

    
    public String sign(Map<String, Object> params) {
        return sign(params, "MD5");
    }

    
    public String sign(Map<String, Object> reqParams, String signType) {
        Map<String, Object> sortMap = new TreeMap<>();
        if (reqParams != null && !reqParams.isEmpty()) {
            sortMap.putAll(reqParams);
        }
        sortMap.put("appid", this.appId);
        sortMap.put("timestamp", System.currentTimeMillis() / 1000);  
        sortMap.put("sign_type", signType);

        StringBuilder sign = new StringBuilder();
        for (Map.Entry<String, Object> e : sortMap.entrySet()) {
            sign.append(e.getKey())
                    .append('=')
                    .append(e.getValue())
                    .append('&');
        }

        final String signUrl = sign.toString() + "sign=";

        
        sign.append(this.appId)
                .append('.')
                .append(this.appSecret);

        if ("MD5".equals(signType)) {
            return signUrl + EncryptUtils.toMD5Hex(sign.toString());
        } else if ("SHA1".equals(signType)) {
            return signUrl + EncryptUtils.toSHA1Hex(sign.toString());
        } else {
            throw new IllegalArgumentException("signType=" + signType);
        }
    }

    
    public JSON httpGet(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            String resp = Objects.requireNonNull(response.body()).string();
            return (JSON) JSON.parse(resp);
        }
    }

    
    public JSON httpPost(String url, JSON post) throws IOException {
        RequestBody body = RequestBody.create(post.toJSONString(), JSON_TYPE);

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            String resp = Objects.requireNonNull(response.body()).string();
            return (JSON) JSON.parse(resp);
        }
    }

    
    public JSON get(String apiName, Map<String, Object> reqParams) {
        final String apiUrl = buildApiUrl(apiName, reqParams);
        try {
            return httpGet(apiUrl);
        } catch (Exception e) {
            LOG.error("Api (GET) failed : " + apiUrl, e);
            return ERROR_REQ;
        }
    }

    
    public JSON post(String apiName, Map<String, Object> reqParams, JSON post) {
        final String apiUrl = buildApiUrl(apiName, reqParams);
        try {
            return httpPost(apiUrl, post);
        } catch (Exception e) {
            LOG.error("Api (POST) failed : " + apiUrl, e);
            return ERROR_REQ;
        }
    }

    private String buildApiUrl(String apiName, Map<String, Object> reqParams) {
        StringBuilder apiUrl = new StringBuilder(baseUrl);
        if (!baseUrl.endsWith("/")) apiUrl.append('/');

        if (apiName.startsWith("/")) apiName = apiName.substring(1);
        apiUrl.append(apiName);

        apiUrl.append('?').append(sign(reqParams));
        return apiUrl.toString();
    }
}
